#!/bin/bash

export UM_RUTA=/home/pi/apps/um_server_react
export UM_SERVER_RUTA=/home/pi/apps/um_server_react/python-server

cd $UM_SERVER_RUTA
#docker login
docker stop python-server_app_1
docker stop python-server_redis_1
docker pull ignaciofalco/umapp:latest
docker-compose up -d
touch $UM_RUTA/finish

