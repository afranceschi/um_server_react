import React, { Component } from 'react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import { Provider } from 'react-redux'

import { store } from './store'
import Dashboard from './Components/Dashboard'

import './App.css'

const muiTheme = createMuiTheme({
	palette: {
		primary: { main: '#9f233f' },
		secondary: { main: '#8a8a8d' }
	},
	typography: {
		useNextVariants: true,
		fontFamily: '"Gotham-Book", sans-serif'
	}
})

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<MuiThemeProvider theme={muiTheme}>
					<Dashboard />
				</MuiThemeProvider>
			</Provider>
		)
	}
}

export default App
