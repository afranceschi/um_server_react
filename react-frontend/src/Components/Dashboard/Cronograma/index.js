import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Delete, BorderColor } from '@material-ui/icons'

import {
	Grid,
	Card,
	CardContent,
	Button,
	TextField,
	CardHeader,
	Typography,
	Divider,
	DialogContent,
	DialogContentText,
	DialogActions,
	Dialog,
	DialogTitle
} from '@material-ui/core'

import { setCronoForm } from './../../../actions/setCronoForm'

import clockLogo from '../../../assets/waiting.png'

const styles = theme => ({
	root: {
		padding: '8px',
		paddingBottom: 20,
		width: '100%',
		height: '84vh'
	},
	button: {
		margin: 0,
		marginLeft: 0,
		marginTop: 20,
		height: 70,
		fontSize: 22,
		textTransform: 'none',
		borderRadius: 10,
		background: 'white',
		color: '#898B8D',
		'&:hover': {
			background: '#9F233F',
			color: 'white'
		},
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px',
		width: '45%'
	},
	icon: {
		marginLeft: 20
	},
	buttonContainer: {
		marginBottom: -5,
		height: '10vh'
	},
	textField: {
		width: '90%',
		fontSize: 40,
		marginTop: '8%',
		marginLeft: '5%',
		marginRight: '5%',
		marginBottom: '8%'
	},
	myLabel: {
		letterSpacing: 2
	},
	input: {
		fontSize: 25
	},
	card: {
		borderRadius: 0
	},
	myLeftCard: {
		height: '100%',
		margin: 0
	},
	left: {
		paddingRight: 20,
		height: '85vh',
		marginTop: 15
	},
	right: {
		overflowY: 'scroll',
		height: '84vh',
		marginTop: 15,
		boxShadow: 'none'
	},
	eventCard: {
		boxShadow: 'none'
	},
	myCardHeader: {
		BorderColor: 'black',
		boxShadow: '1px',
		marginbTop: 1
	},
	myCardContent: {
		widht: '100%',
		padding: 0
	},
	myTypo: {
		fontSize: 16
	},
	myHeaderTypo: {
		fontSize: 30
	},
	myHoraTypo: {
		fontSize: 18,
		paddingTop: '6%',
		paddingBottom: '6%',
		marginRight: '5%',
		textAlign: 'center',
		background: '#4BBC75',
		color: 'white',
		borderRadius: 3,
		border: 'none',
		textDecoration: 'none'
	},
	myTitleTypo: {
		fontSize: 18,
		overflowWrap: 'break-word',
		wordBreak: 'break-word',
		hyphens: 'auto',
		whiteSpace: 'pre-line'
	},
	logo: {
		marginLeft: 20,
		marginRight: 30
	},
	myContainer: {
		margin: 0,
		padding: 0,
		height: '75vh'
	}
})

class Cronograma extends Component {
	constructor() {
		super()
		this.state = {
			open: false,
			titulo: '',
			id: -1
		}
	}

	handleBorrar = () => {
		this.props.setCronoForm('id', -1)
		this.props.setCronoForm('fecha', '2018-01-01')
		this.props.setCronoForm('hora', '07:30')
		this.props.setCronoForm('titulo', '')
		this.props.setCronoForm('texto', '')
	}

	handleGuardar = () => {
		const { form, socket } = this.props
		if (form.fecha && form.hora && form.titulo) {
			socket.send(
				JSON.stringify({
					type: form.id === -1 ? 'set_actividad' : 'update_actividad',
					form
				})
			)
		} else {
			alert('Por favor complete todos los campos del formulario')
		}
	}

	handleChange = name => event => {
		this.props.setCronoForm(name, event.target.value)
	}

	editEvent = (fecha, act) => {
		this.props.setCronoForm('id', act.id)
		this.props.setCronoForm('fecha', fecha)
		this.props.setCronoForm('hora', act.hora)
		this.props.setCronoForm('titulo', act.titulo)
		this.props.setCronoForm('texto', act.texto)
	}

	deleteEvent = (id, titulo) => {
		this.setState({ id, open: true, titulo })
	}

	handleAccept = () => {
		this.props.socket.send(
			JSON.stringify({
				type: 'delete_actividad',
				id: this.state.id
			})
		)
		this.setState({ open: false })
	}

	renderFecha = fecha => {
		const { dias, meses } = this.props
		const aux = fecha.split('-')
		const date = new Date(aux[0], parseInt(aux[1], 10) - 1, aux[2])
		return dias[date.getDay()] + ' ' + date.getDate() + ' de ' + meses[date.getMonth()]
	}

	render() {
		const { classes, form, cronograma } = this.props
		return (
			<Grid container className={classes.root} direction={'row'} justify={'space-between'} alignItems={'center'}>
				<Grid item xs={6} className={classes.left}>
					<div className={classes.myContainer}>
						<Card className={classes.myLeftCard}>
							<TextField
								id="date"
								label={<span className={classes.myLabel}>FECHA</span>}
								type="date"
								className={classes.textField}
								value={form.fecha}
								InputLabelProps={{
									shrink: true
								}}
								InputProps={{
									classes: {
										input: classes.input
									}
								}}
								onChange={this.handleChange('fecha')}
							/>
							<TextField
								id="time"
								label={<span className={classes.myLabel}>HORA</span>}
								type="time"
								className={classes.textField}
								value={form.hora}
								InputLabelProps={{
									shrink: true
								}}
								InputProps={{
									classes: {
										input: classes.input
									}
								}}
								inputProps={{
									step: 300 // 5 min
								}}
								onChange={this.handleChange('hora')}
							/>
							<TextField
								id="standard-name"
								label={<span className={classes.myLabel}>TITULO</span>}
								className={classes.textField}
								value={form.titulo}
								onChange={this.handleChange('titulo')}
								margin="normal"
								InputLabelProps={{
									shrink: true
								}}
								InputProps={{
									classes: {
										input: classes.input
									}
								}}
							/>
							<TextField
								id="standard-name2"
								label={<span className={classes.myLabel}>TEXTO (opcional)</span>}
								className={classes.textField}
								value={form.texto}
								onChange={this.handleChange('texto')}
								margin="normal"
								InputLabelProps={{
									shrink: true
								}}
								InputProps={{
									classes: {
										input: classes.input
									}
								}}
							/>
						</Card>
					</div>
					<Grid
						container
						className={classes.buttonContainer}
						direction={'row'}
						justify={'space-between'}
						alignItems={'center'}
					>
						<Button variant={'contained'} className={classes.button} onClick={() => this.handleBorrar()}>
							BORRAR
						</Button>
						<Button variant={'contained'} className={classes.button} onClick={() => this.handleGuardar()}>
							GUARDAR
						</Button>
					</Grid>
				</Grid>
				<Grid item xs={6} className={classes.right}>
					{cronograma.map(
						(c, i) =>
							c.actividad.length !== 0 ? (
								<Card key={`card-${c.fecha}-1`} className={classes.eventCard}>
									<CardContent key={`content-${c.fecha}-3`} className={classes.myCardContent}>
										<CardHeader
											key={`header-${c.fecha}-2`}
											title={
												<Typography className={classes.myHeaderTypo}>
													<img src={clockLogo} alt='asd' className={classes.logo} />
													{this.renderFecha(c.fecha)}
												</Typography>
											}
											className={classes.myCardHeader}
										/>
										{c.actividad.map((act, j) => (
											<Grid
												key={`${c.fecha}-${act.id}-1`}
												container
												direction={'row'}
												style={{ paddingLeft: '2%', paddingRight: '2%' }}
											>
												<Divider style={{ width: '100%', marginBottom: 20, marginTop: 20 }} />
												<Grid key={`${c.fecha}-${act.id}-2`} item xs={2}>
													<Typography
														className={classes.myHoraTypo}
														key={`${c.fecha}-${act.id}-3`}
														noWrap
													>
														{act.hora} hs
													</Typography>
												</Grid>
												<Grid key={`${c.fecha}-${act.id}-4`} item xs={8}>
													<Typography
														key={`${c.fecha}-${act.id}-5`}
														noWrap
														className={classes.myTitleTypo}
													>
														{act.titulo}
													</Typography>
													<Typography
														key={`${c.fecha}-${act.id}-6`}
														noWrap
														className={classes.myTypo}
													>
														{act.texto}
													</Typography>
												</Grid>
												<Grid key={`${c.fecha}-${act.id}-7`} item xs={2}>
													<BorderColor
														key={`${c.fecha}-${act.id}-8`}
														className={classes.icon}
														onClick={() => this.editEvent(c.fecha, act)}
													/>
													<Delete
														key={`${c.fecha}-${act.id}-9`}
														className={classes.icon}
														onClick={() => this.deleteEvent(act.id, act.titulo)}
													/>
												</Grid>
											</Grid>
										))}
										<Divider style={{ width: '100%', marginBottom: 20, marginTop: 20 }} />
									</CardContent>
								</Card>
							) : null
					)}
				</Grid>
				<Dialog
					open={this.state.open}
					onClose={() => this.setState({ open: false })}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title">{'Eliminar evento'}</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							¿Está seguro que desea eliminar el evento "{this.state.titulo}"?
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={() => this.setState({ open: false })} color="primary">
							No
						</Button>
						<Button onClick={this.handleAccept} color="primary" autoFocus variant="contained">
							Si
						</Button>
					</DialogActions>
				</Dialog>
			</Grid>
		)
	}
}

const mapStateToProps = (state, props) => ({
	socket: state.connection.socket,
	form: state.cronograma.form,
	cronograma: state.cronograma.cron
})

const mapDispatchToProps = {
	setCronoForm
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Cronograma))
