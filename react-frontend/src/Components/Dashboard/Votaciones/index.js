import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { Grid, Button, Card, Typography, Divider, TextField } from '@material-ui/core'

import PaisItem from './PaisItem'
import { setVotacionCron } from './../../../actions/setVotacionCron'
import { setPaises } from './../../../actions/setPaises'
import { setAFavor } from './../../../actions/setAFavor'
import { setEnContra } from './../../../actions/setEnContra'
import { setNoVotaron } from './../../../actions/setNoVotaron'
import { setAbstencion } from './../../../actions/setAbstencion'
import { setIdIntervaloVotacion } from './../../../actions/setIdIntervaloVotacion'

import aFavorLogo from '../../../assets/aFavorLogo.png'
import encontraLogo from '../../../assets/encontraLogo.png'
import abstencionLogo from '../../../assets/abstencionLogo.png'

import { cVerde, cRojo, cGris } from './../../../constants'

const styles = theme => ({
	root: {
		flexGrow: 1,
		height: '88vh'
	},
	left: {
		padding: 0,
		paddingBottom: 5,
		height: '100%',
	},
	right: {
		height: '100%',
		padding: 0,
		paddingBottom: 5
	},
	container: {
		height: '100%'
	},
	button: {
		margin: 15,
		marginLeft: 0,
		marginRight: 50,
		marginTop: 6,
		height: 65,
		fontSize: 18,
		borderRadius: 10,
		background: '#F1F1F1',
		color: '#898B8D',
		textTransform: 'none',
		'&:hover': {
			background: '#9F233F',
			color: 'white'
		},
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px',
		width: '100%'
	},
	card: {
		flex: 1,
		marginTop: 6,
		marginBottom: 0,
		marginLeft: 0,
		marginRight: 50,
		overflow: 'auto',
		maxHeight: '-webkit-fill-available',
	},
	cardRight: {
		flex: 1,
		marginBottom: 0,
		width: '100%'
	},
	cardTitle: {
		padding: 5,
		color: '#898B8D',
		fontSize: 18,
		fontWeight: 100,
		paddingLeft: 28,
	},
	containerTemporizador: {
	},
	buttonTemporizador: {
		margin: 10,
		height: 60,
		width: 300,
		borderRadius: 20,
		fontSize: 30,
		textTransform: 'none',
	},
	tiempo: {
		margin: 2,
		fontSize: 55,
		color: '#898B8D',
	},
	textoTemporizador: {
		width: '90%',
		marginTop: 5
	},
	etiqueta: {
		margin: 10,
		color: '#898B8D',
		fontWeight: 100,
		align: 'center',
		marginLeft: 25,
		fontSize: 12
	},
	myLogo: {
		float: 'none',
		marginLeft: 10,
		marginTop: 5,
		height: 12,
	},
	myPais: {
		align: 'center',
	}
})

class Votaciones extends Component {
	constructor() {
		super()
		this.state = {
			pregunta: '',
			afavor: [],
			encontra: [],
			novotaron: [],
			abstencion: [],
			Paises: [],
			time: 30,
			idInterval: null
		}
	}

	votacion = (pregunta, abs) => {
		this.props.setVotacionCron(30)
		this.props.setAbstencion([])
		this.props.setAFavor([])
		this.props.setEnContra([])
		this.props.setNoVotaron([])
		const opt = abs
			? [
					{ text: 'A Favor', color: cVerde, value: 'yes' },
					{ text: 'En Contra', color: cRojo, value: 'no' },
					{ text: 'Abstencion', color: cGris, value: 'abs' }
				]
			: [ { text: 'A Favor', color: cVerde, value: 'yes' }, { text: 'En Contra', color: cRojo, value: 'no' } ]
		this.props.socket.send(
			JSON.stringify({
				type: 'broadcast_ask',
				subject: 'votacion',
				question: pregunta,
				options: opt
			})
		)
		const idVot = setInterval(() => this.contador(this.props.socket), 500)
		this.props.setIdIntervaloVotacion(idVot)
		this.setState({ idInterval: idVot })
	}

	contador = socket => {
		if (this.props.time === 0) {
			clearInterval(this.state.idInterval)
		} else {
			socket.send(JSON.stringify({ type: 'get_time' }))
		}
	}

	onChangeInput = e => {
		this.setState({ pregunta: e.target.value })
	}

	manualVote = (pais, voto) => {
		this.props.socket.send(
			JSON.stringify({
				type: 'set_manual',
				handler: 'votacion',
				selected: voto === 'A favor' ? 'yes' : voto === 'En contra' ? 'no' : 'abs',
				pais
			})
		)
	}

	render() {
		const { classes, aFavor, enContra, noVotaron, abstencion, time } = this.props
		const { pregunta } = this.state
		return (
			<Grid container className={classes.root} direction={'row'}>
				<Grid item xs={7} className={classes.left}>
					<Grid container direction={'column'} justify={'space-between'} className={classes.container}>
						<Card className={classes.card}>
							<Typography className={classes.cardTitle}> Votaciones por delegacion </Typography>
							<Divider />
							<Grid justify={'flex-start'} container direction={'row'}>
								<Grid item xs={3}>
									<Grid container direction={'column'} justify={'center'} alignItems={'center'}>
										<Typography className={classes.etiqueta}>
											A FAVOR ({aFavor.filter(v => v[1]).length}){' '}
											<img src={aFavorLogo} alt="logo" align="right" className={classes.myLogo} />
										</Typography>
										{aFavor.map(item => (
											item[1]? <PaisItem
												key={item[0] + '-afavor'}
												nombre={item[0]}
												manualVote={this.manualVote}
											/>: null
										))}
									</Grid>
								</Grid>
								<Grid item xs={3}>
									<Grid container direction={'column'}>
										<Typography className={classes.etiqueta}>
											EN CONTRA ({enContra.filter(v => v[1]).length}){' '}
											<img
												src={encontraLogo}
												alt="logo"
												align="right"
												className={classes.myLogo}
											/>
										</Typography>
										{enContra.map(item => (
											item[1]? <PaisItem
											key={item[0] + '-afavor'}
											nombre={item[0]}
											manualVote={this.manualVote}
										/>: null
										))}
									</Grid>
								</Grid>
								<Grid item xs={3}>
									<Grid container direction={'column'}>
										<Typography className={classes.etiqueta}>
											ABSTENCION ({abstencion.filter(v => v[1]).length}){' '}
											<img
												src={abstencionLogo}
												alt="logo"
												align="right"
												className={classes.myLogo}
											/>
										</Typography>
										{abstencion.map(item => (
											item[1]? <PaisItem
											key={item[0] + '-afavor'}
											nombre={item[0]}
											manualVote={this.manualVote}
										/>: null										
										))}
									</Grid>
								</Grid>
								<Grid item xs={3}>
									<Grid container direction={'column'}>
										<Typography className={classes.etiqueta}>
											NO VOTARON ({noVotaron.filter(v => v[1]).length})
										</Typography>
										{noVotaron.map(item =>
											item[1]? <PaisItem
											key={item[0] + '-afavor'}
											nombre={item[0]}
											manualVote={this.manualVote}
										/>: null
										)}
									</Grid>
								</Grid>
							</Grid>
						</Card>
					</Grid>
				</Grid>
				<Grid item xs={5} className={classes.right}>
					<Grid container direction={'column'} justify={'space-between'} className={classes.container}>
						<Button
							variant="contained"
							className={classes.button}
							onClick={() => this.votacion('Informal de la enmienda', false)}
						>
							{' '}
							Informal de la enmienda{' '}
						</Button>
						<Button
							variant="contained"
							className={classes.button}
							color="primary"
							onClick={() => this.votacion('Formal de la enmienda', true)}
						>
							{' '}
							Formal de la enmienda{' '}
						</Button>
						<Button
							variant="contained"
							className={classes.button}
							onClick={() => this.votacion('Anteproyecto de resolución a proyecto de resolución', true)}
						>
							{' '}
							Anteproyecto de resolucion a proyecto de resolucion{' '}
						</Button>
						<Button
							variant="contained"
							className={classes.button}
							onClick={() => this.votacion('Proyecto de resolución a resolución', true)}
						>
							{' '}
							Proyecto de resolucion a resolucion{' '}
						</Button>
						<Card className={classes.cardRight}>
							<Typography className={classes.cardTitle}> VOTACION </Typography>
							<Divider />
							<Grid
								className={classes.containerTemporizador}
								justify={'space-evenly'}
								alignItems={'center'}
								container
								direction={'column'}
							>
								<TextField
									value={pregunta}
									onChange={this.onChangeInput}
									className={classes.textoTemporizador}
									placeholder={'Asunto'}
									variant='outlined'
								/>
								<Button
									variant="contained"
									className={classes.buttonTemporizador}
									color="primary"
									onClick={() => this.votacion(this.state.pregunta, true)}
								>
									{' '}
									Iniciar{' '}
								</Button>
								<Typography className={classes.tiempo}> 00:{time < 10 ? `0${time}` : time} </Typography>
							</Grid>
						</Card>
					</Grid>
				</Grid>
			</Grid>
		)
	}
}

const MapStateToProps = (state, props) => ({
	socket: state.connection.socket,
	paises: state.paises.list,
	aFavor: state.votacion.aFavor,
	enContra: state.votacion.enContra,
	abstencion: state.votacion.abs,
	noVotaron: state.votacion.noVotaron,
	time: state.votacion.time,
	idIntervalo: state.votacion.idInterval
})

const MapDispatchToProps = {
	setPaises,
	setAFavor,
	setEnContra,
	setNoVotaron,
	setAbstencion,
	setVotacionCron,
	setIdIntervaloVotacion
}

export default connect(MapStateToProps, MapDispatchToProps)(withStyles(styles)(Votaciones))
