import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import { Grid, Typography } from '@material-ui/core'
import ArrowMenu from './ArrowMenu'

// import {IndeterminateCheckBox} from '@material-ui/icons';

const styles = {
	root: {
		padding: 15,
		width: '100%'
		//backgroundColor: 'black',
	},
	removeIcon: {
		color: 'red',
		fontSize: 18
	},
	inactive: {
		color: 'red'
	},
	active: {
		color: 'green'
	},
	myTypo: {
		color: '#898B8D',
		fontWeight: 100
		// marginLeft: 22,
	}
}

class PaisItem extends Component {
	constructor() {
		super()
		this.state = {
			status: 'inactive'
		}
	}

	render() {
		const { classes, nombre, manualVote, handleClose } = this.props

		var clase = ''

		// if(estado !== 'active'){
		//     clase = classNames(classes.root, classes['inactive'])
		// }else{
		clase = classNames(classes.root, classes['active'])
		// }

		return (
			<Grid className={clase} item xs={12}>
				<Grid container direction={'row'} justify={'space-between'} alignItems={'center'}>
					<Grid item xs={10}>
						<Typography className={classes.myTypo}> {nombre} </Typography>
					</Grid>
					{manualVote ? (
						<Grid item xs={2}>
							<ArrowMenu
								options={[ 'A favor', 'En contra', 'Abstencion' ]}
								handleClick={voto => manualVote(nombre, voto)}
								handleClose={handleClose}
							/>
						</Grid>
					) : null}
				</Grid>
			</Grid>
		)
	}
}

PaisItem.propTypes = {
	classes: PropTypes.object.isRequired,
	nombre: PropTypes.string.isRequired,
	estado: PropTypes.string
}

export default withStyles(styles)(PaisItem)
