import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Card, Grid, Divider, Typography, CardHeader, CardContent } from '@material-ui/core';
import { Share } from '@material-ui/icons'

import { setEventos } from './../../../actions/setEventos'
import { openSocket } from './../../../actions/openSocket'

const styles = theme => ({
    root: {
        width: '100%',
        padding: '4px',
        overflowY: 'scroll',
        height: '85vh'
    },
    card: {
        margin: '15px',
        padding: 0,
        borderRadius: 7,
    },
    cardContent: {
        padding: 0,
    },
    divs: {
        height: 80,
    },
    header: {
        textAlign: 'center',
        color: '#898B8D',
        fontWeight: 500,
        fontSize: 25,
        letterSpacing: 2,
    },
    resultado: {
        textAlign: 'center',
        fontSize: 40,
        fontWeight: 600,
        color: 'white',
        background: '#4BBC75',
    },
    myTypo1: {
        color: '#4BBC75',
        fontSize: 50,
        fontWeight: 500,
        float: 'left',
        textAlign: 'end',
        width: '45%',
    },
    myTypo2: {
        display: 'inline-block',
        marginLeft: 20,
        marginBottom: 5,
        color: '#4BBC75',
        fontSize: 35,
        marginTop: 10,
    },
    myTypo3: {
        color: '#C92A2A',
        fontSize: 50,
        fontWeight: 500,
        float: 'left',
        textAlign: 'end',
        width: '45%',
    },
    myTypo4: {
        display: 'inline-block',
        marginLeft: 20,
        marginBottom: 5,
        color: '#C92A2A',
        fontSize: 35,
        marginTop: 10,
    },
    myTypo5: {
        color: '#898B8D',
        fontSize: 50,
        fontWeight: 500,
        float: 'left',
        textAlign: 'end',
        width: '45%',
    },
    myTypo6: {
        display: 'inline-block',
        marginLeft: 20,
        marginBottom: 5,
        color: '#898B8D',
        fontSize: 35,
        marginTop: 10,
    },
});

class Eventos extends Component {

    componentDidMount() {
        const { socket } = this.props;
        if (socket && socket.readyState === WebSocket.OPEN)
            socket.send(JSON.stringify({type: 'get_events'}));
    }

    handleRecive = data => {
        data = JSON.parse(data);
        if (data.subject === 'get_events') {
            this.props.setEventos(data.list);
        }
    }

    handleShare = (id) => {
        this.props.socket.send(JSON.stringify({
            type: 'share_event',
            id,
        }))
    }

    render() {
        const { classes, eventos } = this.props;
        return(
            <div className={classes.root}>
                <Grid container alignItems={'stretch'}
                    direction={'row'} justify={'center'}>
                    {eventos ? eventos.map((evento, i) => (
                        <Grid key={`${i}-grkey`} item xs={12}>
                            <Card key={`${i}-card`} className={classes.card}>
                                <CardHeader key={`${i}-cardHeader`} className={classes.header} title={<Typography className={classes.header}>{evento.asunto} {evento.mostrar === 0? <Share style={{color:'#4BBC75'}} onClick={() => this.handleShare(evento.id)}/> : null} </Typography>}/>
                                <Divider></Divider>
                                <CardContent key={`${i}-cardContent`} className={classes.cardContent}>
                                    <div className={classes.divs}>
                                    {<Typography key={`${i}-typo2`} className={classes.myTypo1}>
                                        {`${evento.aFavor}%`}
                                    </Typography>}
                                    <Typography className={classes.myTypo2}>
                                         A FAVOR
                                    </Typography> 
                                    </div>
                                    <div className={classes.divs}>
                                    <Typography key={`${i}-typo3`} className={classes.myTypo3}>
                                        {`${evento.enContra}%`}
                                        </Typography>      
                                    <Typography className={classes.myTypo4}>
                                        EN CONTRA
                                    </Typography>
                                    </div>
                                    <div className={classes.divs}>
                                    <Typography key={`${i}-typo4`} className={classes.myTypo5}>
                                        {`${evento.abs}%`}
                                    </Typography>                      
                                    <Typography className={classes.myTypo6}>
                                        ABSTENCIONES
                                    </Typography>
                                    </div>                                                                                                                 
                                </CardContent>
                            </Card>
                        </Grid>
                    )) : <Typography variant={'display1'}>No hay eventos para mostrar</Typography>} 
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => ({
    socket: state.connection.socket,
    eventos: state.eventos.list,
});

const mapDispatchToProps = {
    setEventos,
    openSocket,
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Eventos))