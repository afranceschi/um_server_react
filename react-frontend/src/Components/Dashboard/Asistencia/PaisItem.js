import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import {
    Grid,
    Typography,
    IconButton,
   // ButtonBase,
} from '@material-ui/core';

import {IndeterminateCheckBox, AddBox} from '@material-ui/icons';


const styles = theme => ({
    root: {
        margin: '5px 20px',
    },
    removeIcon: {
        color: '#810C26',
        fontSize: 18,
        //: 'Gotham Book',
    },
    addIcon: {
        color: '#4BBC75',
        fontSize: 18,
        //: 'Gotham Book',
    },
    inactive: {
        color: '#C92A2A',
        //: 'Gotham Book',
    },
    active: {
        color: '#4BBC75',
        //: 'Gotham Book',
    },
    manual: {
        color: '#000000',
        //: 'Gotham Book',
    }
})

class PaisItem extends Component{

    constructor(){
        super();
        this.state = {
            status: 'inactive',
        }
    }

    render(){

        const {classes, nombre, estado, click, manual} = this.props;
        var clase = '';
        if(estado !== 1){
                clase = classNames(classes.root, classes['inactive']);
        }else{
            if (manual === 1)
                clase = classNames(classes.root, classes['manual']);
            else
                clase = classNames(classes.root, classes['active'])
        }

        return(
            <Grid className={clase} item xs={12}>
                <Grid container direction={'row'} wrap={"nowrap"} justify={'space-between'} alignItems={'center'}>
                    <Typography color={'inherit'}> {nombre} </Typography>
                    <IconButton onClick={()=>click(nombre)}>

                    { estado === 1?                        
                        <IndeterminateCheckBox  className={classes.removeIcon} />
                        :
                        <AddBox className={classes.addIcon} />
                    }
                    </IconButton>
                </Grid>
            </Grid>
        );
    }
}

PaisItem.propTypes = {
    classes: PropTypes.object.isRequired,
    nombre: PropTypes.string.isRequired,
    estado: PropTypes.number,
};

export default withStyles(styles)(PaisItem)