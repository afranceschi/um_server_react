import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles, InputAdornment } from '@material-ui/core'
import { Grid, Card, Divider, Typography, TextField } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

import PaisItem from './PaisItem'
import { openSocket } from './../../../actions/openSocket'
import { setPaises } from './../../../actions/setPaises'

const styles = theme => ({
	root: {
		display: 'flex',
		flexGrow: 1,
		maxHeight: '100%',
		overflowY: 'auto'
	},
	rootRight: {
		padding: 0,
		margin: 0
	},
	card: {
		flex: 1,
		bottom: 100,
		margin: 10,
		paddingTop: 10,
		overflowY: 'auto',
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px'
	},
	paisesConectados: {
		maxHeight: '80vh',
		overflow: 'auto'
	},
	card2: {
		flex: 1,
		marginTop: 0,
		textAlign: 'center',
		padding: 10,
		paddingTop: 0,
		paddingLeft: 50,
		paddingRight: 50
	},
	card3: {
		flex: 1,
		marginBottom: 0,
		paddingTop: 10,
		overflowY: 'auto',
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px'
	},
	paisesAusentes: {
		maxHeight: '55vh',
		overflow: 'auto'
	},
	cardTitle: {
		paddingBottom: 10,
		paddingLeft: 10,
		fontSize: 18,
		color: '#898B8D'
	},
	typoNumero: {
		fontSize: 32,
		color: '#898B8D',
		marginRight: 40
	},
	test: {
		marginTop: 10,
		fontSize: 18,
		color: '#898B8D'
	}
})

class Asistencia extends React.Component {
	constructor() {
		super()
		this.state = {
			search: ''
		}
	}

	IntToStr = data => {
		var output = ''
		if (data < 100) {
			output += '0'
			if (data < 10) {
				output += '0'
				output += data.toString()
			} else {
				output += data.toString()
			}
		} else {
			output += data.toString()
		}
		return output
	}

	handleDisconnect = pais => {
		this.props.socket.send(
			JSON.stringify({
				type: 'set_manual',
				handler: 'asistencia',
				name: pais,
				action: 'disconnect'
			})
		)
	}

	handleConnect = pais => {
		this.props.socket.send(
			JSON.stringify({
				type: 'set_manual',
				handler: 'asistencia',
				name: pais,
				action: 'connect'
			})
		)
	}

	handleChange = event => {
		this.setState({ search: event.target.value })
	}

	render() {
		const { classes, paises } = this.props
		const { search } = this.state
		const total = this.IntToStr(paises.length)
		const presentes = this.IntToStr(paises.filter(pais => pais.connected === 1).length)
		const ausentes = this.IntToStr(total - presentes)
		return (
			<Grid className={classes.root} container direction={'row'}>
				<Grid item xs={8}>
					<Card className={classes.card}>
						<Typography className={classes.cardTitle}> Delegaciones </Typography>
						<Divider />
						<Grid
							className={classes.paisesConectados}
							justify={'flex-start'}
							alignItems={'center'}
							container
							direction={'row'}
						>
							{paises ? (
								paises.map(
									pais =>
										pais.connected ? (
											<Grid key={pais.name} item xs={3}>
												<PaisItem
													key={pais.name}
													nombre={pais.name}
													estado={pais.connected}
													manual={pais.manual}
													click={this.handleDisconnect}
												/>
											</Grid>
										) : null
								)
							) : null}
						</Grid>
					</Card>
				</Grid>
				<Grid item xs={4} className={classes.rootRight}>
					<Grid
						container
						direction={'column'}
						justify={'space-between'}
						alignItems={'stretch'}
						style={{ paddingRight: 3, paddingBottom: 3 }}
					>
						<div className={classes.card2}>
							<Typography className={classes.cardTitle}> CANT. DE DELEGACIONES </Typography>
							<Grid container direction={'row'} justify={'space-between'} alignItems={'center'}>
								<Typography className={classes.test}> PRESENTES </Typography>
								<Typography className={classes.typoNumero}> {presentes} </Typography>
							</Grid>
							<Grid container direction={'row'} justify={'space-between'} alignItems={'center'}>
								<Typography className={classes.test}> AUSENTES </Typography>
								<Typography className={classes.typoNumero}> {ausentes} </Typography>
							</Grid>
							<Grid container direction={'row'} justify={'space-between'} alignItems={'center'}>
								<Typography className={classes.test}> TOTAL </Typography>
								<Typography className={classes.typoNumero}> {total} </Typography>
							</Grid>
						</div>
						<Card className={classes.card3}>
							<Typography className={classes.cardTitle}> Listado de ausentes </Typography>
							<Divider />
							<div style={{ width: '90%', padding: 8 }}>
								<TextField
									placeholder={'Buscar'}
									onChange={this.handleChange}
									value={search}
									fullWidth
									variant="outlined"
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesomeIcon icon={faSearch} style={{color: 'grey'}}/>
											</InputAdornment>
										)
									}}
								/>
							</div>

							<Grid className={classes.paisesAusentes} justify={'flex-start'} container direction={'row'}>
								{paises ? (
									paises.map(pais => {
										if (
											pais.connected !== 1 &&
											pais.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
										)
											return (
												<Grid key={pais.name} item xs={12}>
													<PaisItem
														key={pais.name}
														nombre={pais.name}
														estado={pais.connected}
														manual={pais.manual}
														click={this.handleConnect}
													/>
												</Grid>
											)
										return null
									})
								) : null}
							</Grid>
						</Card>
					</Grid>
				</Grid>
			</Grid>
		)
	}
}

Asistencia.propTypes = {
	classes: PropTypes.object.isRequired
}

const mapStateToProps = (state, props) => ({
	socket: state.connection.socket,
	paises: state.paises.list
})

const mapDispatchToProps = {
	openSocket,
	setPaises
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Asistencia))
