import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Typography, Button, Divider} from '@material-ui/core';
import Form from './Form';
import CornerMenu from './CornerMenu';

import pdfLogo from '../../../assets/Asset1.png';

const styles = theme => ({
    root: {
        padding: '4px',
        width: '100%',
        height: '100%',
    },
    card: {
        margin: '6px',
    },
    centerContent: {
        justifyContent: 'center',
    },
    myButton: {
        fontSize: 12,
        lineHeight: 'inherit',
        paddingBottom: 20,
        paddingTop: 5,
        borderRadius: 30,
        width: '80%',
        minHeight: 23,
        height: 25,
        textTransform: 'none',
    },
    tituloText: {
        color: theme.palette.primary.main,
        marginRight: 20,
        fontSize: 17,
        marginBottom: 35,
        overflowWrap: 'break-word',
        wordBreak: 'break-word',
        hyphens: 'auto',
    },
    uploadButton: {
        width: 250,
        fontSize: 18,
        textTransform: 'none',
        float: 'right',
        borderRadius: 30,
        marginBottom: 30,
        marginRight: 5,
        boxShadow: 'none'
    },
    pdfLogo: {
        height: 140,
        width: 120,
        marginBottom: 20,
    },
    myMui: {
        height: '100%',
    },
})

class Reglamento extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            anteproyecto: false,
        }
    }

    componentDidMount() {
        if (this.props.socket && this.props.socket.readyState === WebSocket.OPEN){
            this.props.socket.send(JSON.stringify({
                type: 'get_documents_list',
            }))
        }
    }

    deleteReglamento = titulo => {
        this.props.socket.send(JSON.stringify({
            type: 'delete_document',
            titulo,
        }))
    }
    
    render() {
        const { classes, documentos } = this.props;
        return(
            <div className={classes.root}>
                <div>
                    <Button 
                        className={classes.uploadButton}
                        onClick={() => this.setState({open: true, anteproyecto: false})}
                        color={'primary'}
                        variant={'contained'}
                    >
                        Subir reglamento
                    </Button>
                    <Button 
                        className={classes.uploadButton}
                        onClick={() => this.setState({open: true, anteproyecto: true})}
                        color={'primary'}
                        variant={'contained'}
                    >
                        Subir Anteproyecto
                    </Button>
                </div>
                <div style={{ width: '100%', textAlign:'-webkit-center' }} >
                    <Divider style={{ width: '98%', background:'grey' }} />
                </div>
                <Grid container alignItems={'stretch'}
                    direction={'row'} justify={'flex-start'} style={{overflowY:'scroll', maxHeight: '80vh'}}>
                    {documentos && documentos.length !==0 ?
                        documentos.map((doc, i) => (
                            <Grid key={`${i}-grid`} item xs={2} style={{padding:15}}>
                                <Grid key={`${i}-grid2`} container alignItems={'center'}
                                    direction={'column'} justify={'space-between'} className={classes.myMui}>
                                    <div style={{display:'inline-flex'}}>
                                        <Typography key={`${i}-cardHeader`} className={classes.tituloText}>{doc.titulo} </Typography>
                                        <CornerMenu options={['Eliminar']} handleClick={() => {this.deleteReglamento(doc.titulo);this.setState({open:false})}}></CornerMenu>
                                    </div>
                                    <div>
                                        <img src={pdfLogo} alt='logo' className={classes.pdfLogo}></img>
                                        <span>
                                            <Button variant={'contained'}
                                                key={`${i}-button`}
                                                color={'primary'}
                                                onClick={() => window.open(doc.link)}
                                                className={classes.myButton}
                                                >
                                                Abrir
                                            </Button>
                                        </span>
                                    </div>
                                </Grid>
                            </Grid>
                    )) :
                        <Typography style={{color: '#7a7a7a'}}>
                            No hay documentos para mostrar.
                        </Typography> }
                </Grid>
                <Form 
                    open={this.state.open} 
                    socket={this.props.socket} 
                    showSnack={this.props.showSnack} 
                    onClose={() => this.setState({open: false})} 
                    anteproyecto={this.state.anteproyecto}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, props) => ({
    socket: state.connection.socket,
    documentos: state.reglamento.list,
})

export default connect(mapStateToProps, null)(withStyles(styles)(Reglamento))