import React, { Component } from 'react'
//import { connect } from 'react-redux'
import { Dialog, DialogTitle, DialogContent, DialogActions, TextField, Typography, Button } from '@material-ui/core';

class Form extends Component {
    constructor(){
        super();
        this.state = {
            titulo: '',
        }
    }

    handleChange = event => {
        this.setState({titulo: event.target.value});
    }

    encodeBase64 = (file, onLoadCallback) => {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = onLoadCallback;
    }

    handleSend = () => {
        var { socket, anteproyecto } = this.props;
        const { titulo } = this.state;
        if ((titulo || anteproyecto) && document.getElementById('path').files[0]) {
            if (document.getElementById('path').value.endsWith('.pdf')) {
                this.encodeBase64(document.getElementById('path').files[0], (e) => {
                    socket.send(JSON.stringify({
                        type: 'post_document',
                        documento: e.target.result,
                        titulo: anteproyecto? 'Anteproyecto de Resolucion' : titulo,
                    }))
                    this.props.onClose();
                });
            } else 
                this.props.showSnack('Seleccione un documento en formato PDF');
        } else {
            this.props.showSnack('Uno o mas campos estan vacíos');
        }
    }

    render(){
        var { titulo } = this.state;
        if (this.props.anteproyecto){
            titulo = 'Anteproyecto de Resolucion';
        }
        return(
            <Dialog
                open={this.props.open}
            >
                <DialogTitle>
                    <Typography>
                        Cargar nuevo documento
                    </Typography>
                </DialogTitle>
                <DialogContent>
                    <TextField
                        fullWidth
                        placeholder={'Nombre'}
                        style={{margin: '5px 0px'}}
                        value={titulo}
                        onChange={this.handleChange}
                        disabled={this.props.anteproyecto}
                        variant='outlined'
                        />
                    <TextField
                        id={'path'}
                        fullWidth
                        //onChange={(event) => console.log(event.target.value)}
                        variant='outlined'
                        inputProps={{
                            type: 'file',
                            name: 'pdf input',
                            accept: 'application/pdf',
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.onClose}>
                      Cerrar
                    </Button>
                    <Button variant='contained' onClick={() => this.handleSend()} >
                        Enviar
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default Form;
