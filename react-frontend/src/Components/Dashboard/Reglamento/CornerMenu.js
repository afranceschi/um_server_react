import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { MoreVert } from '@material-ui/icons/';

const ITEM_HEIGHT = 48;

class CornerMenu extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false,
            anchorEl: null,
        }
    }

    render() {
        const { handleClick, options } = this.props;
        const { open, anchorEl } = this.state;
        return (
            <div>
                <MoreVert onClick={(event) => this.setState({ open: true, anchorEl: event.currentTarget })} />
                <Menu
                    anchorEl={anchorEl}
                    id="long-menu"
                    open={open}
                    onClose={() => this.setState({open: false})}
                    PaperProps={{
                        style: {
                          maxHeight: ITEM_HEIGHT * 4.5,
                          width: 200,
                        },
                    }}
                >
                    {options.map(option => (
                        <MenuItem key={option} onClick={() => handleClick()}>
                            {option}
                        </MenuItem>
                    ))}
                </Menu>
            </div>
        );
    }
}

export default CornerMenu;