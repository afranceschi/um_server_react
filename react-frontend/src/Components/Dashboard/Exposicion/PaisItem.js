import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import {
   // Grid,
    Typography,
    ButtonBase,
} from '@material-ui/core';

//import {IndeterminateCheckBox} from '@material-ui/icons';


const styles = theme => ({
    root: {
        margin: 20,
    },
    removeIcon: {
        color: theme.palette.primary.main,
        fontSize: 18,
    },
    inactive: {
        color: 'black',
    },
    active: {
        color: 'green',
    },
    typoActive: {
        color: 'green',
        fontSize: '10',
        fontWeight: 'bold',
    },
    typoInactive: {
        color: theme.palette.primary.main,
        fontSize: '10',
        fontWeight: 'normal',
    }
})

class PaisItem extends Component{

    constructor(){
        super();
        this.state = {
            status: 'inactive',
        }
    }

    render(){

        const {classes, nombre, spoken, handleOnClick} = this.props;

        //var clase = '';
        var typoClase = '';

        if(spoken === 0){
            //clase = classNames(classes.root, classes['active']);
            typoClase = classNames(classes.root, classes['typoActive']);
        }else{
            //clase = classNames(classes.root, classes['inactive'])
            typoClase = classNames(classes.root, classes['typoInactive']);
        }

        return(
            <ButtonBase style={{width:'100%'}} key={nombre + '-button'} disabled={spoken === 0? false: true} onClick={() => handleOnClick(nombre)}>
                {/* <Grid key={nombre + '-gridContainer'} container direction={'row'} justify={'space-between'}>
                    <Grid key={nombre + '-gridItem'} className={clase} item xs={12}> */}
                        <Typography key={nombre + '-typo'} color={'inherit'} className={typoClase}> {nombre} </Typography>
                        {/* <IndeterminateCheckBox key={nombre + '-icon'} className={classes.removeIcon} /> */}
                    {/* </Grid>
                </Grid> */}
            </ButtonBase>
        );
    }
}

PaisItem.propTypes = {
    classes: PropTypes.object.isRequired,
    nombre: PropTypes.string.isRequired,
    estado: PropTypes.number,
};

export default withStyles(styles)(PaisItem);