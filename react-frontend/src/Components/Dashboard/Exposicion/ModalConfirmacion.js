import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { withStyles, TextField } from '@material-ui/core'
import { cVerde, cRojo } from './../../../constants'

const styles = theme => ({
	root: {
		maxHeight: '80vh'
	}
})

class ModalConfirmacion extends Component {
	constructor() {
		super()
		this.state = {
			checked: [],
			asunto: ''
		}
	}

	listaOradores = () => {
		this.props.setOradores([])
		this.props.socket.send(
			JSON.stringify({
				type: 'broadcast_ask',
				subject: 'oradores',
				question: this.state.asunto,
				options: [ { text: 'SI', color: cVerde, value: 'yes' }, { text: 'NO', color: cRojo, value: 'no' } ]
			})
		)
		this.props.done()
	}

	handleToggle = pais => {
		var newChecked = [ ...this.state.checked ]
		if (this.state.checked.indexOf(pais) !== -1) newChecked.pop(pais)
		else newChecked.push(pais)
		this.setState({ checked: newChecked })
	}

	handleSubmit = () => {
		this.listaOradores()
	}

	handleChange = event => {
		this.setState({ asunto: event.target.value })
	}

	render() {
		var { classes, open, closeModal } = this.props
		if (open === undefined) open = false
		return (
			<div className={classes.root}>
				<Dialog open={open} onClose={closeModal} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Nueva lista de oradores</DialogTitle>
					<DialogContent>
						<TextField
							placeholder={'Asunto'}
							onChange={this.handleChange}
							fullWidth
							variant="outlined"
							color="primary"
						/>
					</DialogContent>
					<DialogActions>
						<Button onClick={closeModal} color="default">
							Cancelar
						</Button>
						<Button variant="contained" onClick={this.handleSubmit} color="primary">
							Agregar
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		)
	}
}

export default withStyles(styles)(ModalConfirmacion)
