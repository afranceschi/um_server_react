import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { withStyles, List, ListItem, Checkbox, ListItemText, TextField, InputAdornment } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const styles = theme => ({
	root: {
		maxHeight: '80vh'
	}
})

class Modal extends Component {
	constructor() {
		super()
		this.state = {
			checked: [],
			search: ''
		}
	}

	handleToggle = pais => {
		var newChecked = [ ...this.state.checked ]
		if (this.state.checked.indexOf(pais) !== -1) newChecked.pop(pais)
		else newChecked.push(pais)
		this.setState({ checked: newChecked })
	}

	handleSubmit = () => {
		//console.log(this.props.idVotacion)
		this.props.sendOradoresList(this.state.checked, this.props.idVotacion)
		this.setState({ checked: [], search: '' })
		this.props.closeModal()
	}

	handleChange = event => {
		this.setState({ search: event.target.value })
	}

	toSimpleLower = s => {
		s = s.toLowerCase()
		s = s.replace('á', 'a')
		s = s.replace('é', 'e')
		s = s.replace('í', 'i')
		s = s.replace('ó', 'o')
		s = s.replace('ú', 'u')
		return s
	}

	render() {
		var { classes, open, closeModal, paises } = this.props
		const { search } = this.state
		if (open === undefined) open = false
		return (
			<div className={classes.root}>
				<Dialog open={open} onClose={closeModal} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">
						Agregar paises a la lista de oradores manualmente
						<TextField
							placeholder={'Buscar'}
							onChange={this.handleChange}
							value={search}
							fullWidth
							variant="outlined"
							InputProps={{
								startAdornment: (
									<InputAdornment>
										<FontAwesomeIcon icon={faSearch} style={{ color: 'grey', marginRight: 5 }} />
									</InputAdornment>
								)
							}}
						/>
					</DialogTitle>
					<DialogContent>
						<List>
							{paises.map(
								pais =>
									this.toSimpleLower(pais.name).indexOf(this.toSimpleLower(search)) !== -1 ? (
										<ListItem
											key={pais.name + '-listitem'}
											color={'primary'}
											role={undefined}
											button
											onClick={() => this.handleToggle(pais)}
										>
											<Checkbox
												color="primary"
												key={pais.name + '-listcheckbox'}
												checked={this.state.checked.indexOf(pais) !== -1}
												tabIndex={-1}
												disableRipple
											/>
											<ListItemText key={pais.name + '-listitemtext'} primary={pais.name} />
										</ListItem>
									) : null
							)}
						</List>
					</DialogContent>
					<DialogActions>
						<Button onClick={closeModal} color="default">
							Cancelar
						</Button>
						<Button variant="contained" onClick={this.handleSubmit} color="primary">
							Agregar
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		)
	}
}

// const mapStateToProps = (state, props) => ({
// })

// const mapDispatchToProps = {
// }

export default withStyles(styles)(Modal)
