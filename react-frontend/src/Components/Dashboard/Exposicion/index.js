import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp, faArrowDown, faPause, faUndo } from '@fortawesome/free-solid-svg-icons'

import PaisItem from './PaisItem'
import { openSocket } from './../../../actions/openSocket'
import { setOradores } from './../../../actions/setOradores'
import Modal from './Modal'
import ModalConfirmacion from './ModalConfirmacion'

import { Grid, Button, Card, Typography, Divider, MenuItem } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { TextField } from '@material-ui/core'

const styles = theme => ({
	root: {
		flexGrow: 1,
		height: `calc(100% - 120px)`,
	},
	left: {
		padding: 10,
		maxWidth: '35%'
	},
	right: {
		padding: 10,
		maxWidth: '65%',
		flexBasis: '65%'
	},
	container: {
		height: '100%',
		width: '100%'
	},
	button: {
		margin: 15,
		marginLeft: 0,
		marginRight: 50,
		marginTop: 6,
		height: 75,
		fontSize: 22,
		background: '#F1F1F1',
		color: '#898B8D',
		textTransform: 'none',
		'&:hover': {
			background: '#9F233F',
			color: 'white'
		},
		borderRadius: 10,
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px',
		width: '100%'
	},
	card: {
		flex: 1,
		margin: 15,
		marginBottom: 0,
		marginLeft: 0,
		marginRight: 100,
		width: '100%',
	},
	cardRight: {
		flex: 1,
		marginTop: '1vh',
		marginBottom: 0,
		minHeight: '79vh',
	},
	cardTitle: {
		padding: 10,
		color: '#898B8D',
	},
	containerTemporizador: {
		width: '100%',
	},
	buttonTemporizador: {
		margin: 10,
		marginTop: 80,
		height: 80,
		// width: '80%',
		textTransform: 'none',
		borderRadius: 15,
		fontSize: 40,
	},
	tiempo: {
		margin: 20,
		fontSize: 64,
		width: '100%',
		textAlign: 'center',
		color: '#898B8D',
	},
	optionButtons: {
		textTransform: 'none',
		margin: '0px 10px',
		width: '25%'
	}
})

class Exposicion extends Component {
	constructor() {
		super()
		this.state = {
			orador_actual: '',
			temporizador: null,
			tiempo: 120,
			intervalId: -1,
			restante: 0,
			idVotacion: '-1'
		}
	}

	handleOnClick = paisName => {
		this.setState({ orador_actual: paisName })
	}

	checkTime = () => {
		if (this.state.temporizador - new Date().getTime() / 1000 + this.state.tiempo < 0) {
			clearInterval(this.state.intervalId)
			this.setState({ restante: 0 })
		} else if (Math.trunc(this.state.temporizador - new Date().getTime() / 1000 + this.state.tiempo) < 10) {
			this.setState({
				restante:
					'0' +
					Math.trunc(this.state.temporizador - new Date().getTime() / 1000 + this.state.tiempo).toString()
			})
		} else {
			this.setState({
				restante: Math.trunc(this.state.temporizador - new Date().getTime() / 1000 + this.state.tiempo)
			})
		}
	}

	handleInit = () => {
		clearInterval(this.state.intervalId)
		var { orador_actual } = this.state
		const { oradores } = this.props
		const newOradoresList = oradores.map(
			pais => (pais.name === orador_actual ? { name: pais.name, spoken: 1 } : pais)
		)
		this.props.socket.send(JSON.stringify({ type: 'set_spoken', name: orador_actual, id: this.state.idVotacion }))
		this.setState({ temporizador: new Date().getTime() / 1000 })
		this.props.setOradores(newOradoresList)
		const intervalId = setInterval(this.checkTime, 200)
		this.setState({ intervalId })
	}

	handlePause = () => {
		clearInterval(this.state.intervalId)
		this.setState({ intervalId: -1 })
		this.setState({ tiempo: this.state.restante })
	}

	handleRestart = () => {
		clearInterval(this.state.intervalId)
		this.setState({ intervalId: -1 })
		this.setState({ tiempo: 120, restante: 120 })
	}

	agregadoManual = () => {
		if (this.state.idVotacion > 1) this.setState({ modalOpen: true })
		else alert('Seleccione una lista de oradores primero')
	}

	nuevaLista = () => {
		this.setState({ modal1Open: true })
	}

	handleChangeSelect = event => {
		this.setState({ idVotacion: event.target.value, orador_actual: '' })
		//console.log(event)
		this.props.socket.send(
			JSON.stringify({
				type: 'get_speakers',
				id: event.target.value
			})
		)
		this.handleRestart()
	}

	sendOradoresList = (list, id) => {
		for (var i = 0; i < list.length; i++)
			this.props.socket.send(
				JSON.stringify({
					type: 'set_manual',
					handler: 'oradores',
					pais: list[i].name,
					id: id
				})
			)
	}

	sumar = () => {
		const { restante, tiempo } = this.state
		this.setState({ tiempo: tiempo + 60 < 10 * 60 ? tiempo + 60 : tiempo })
		if (restante !== 0) this.setState({ restante: restante + 60 < 10 * 60 ? restante + 60 : restante })
	}

	restar = () => {
		const { restante, tiempo } = this.state
		this.setState({ tiempo: tiempo - 60 > 0 ? tiempo - 60 : tiempo })
		if (restante !== 0) this.setState({ restante: restante - 60 > 0 ? restante - 60 : restante })
	}

	render() {
		const { classes, paises, oradores, listas } = this.props
		const { orador_actual, restante, tiempo } = this.state
		const paises_oradores_name = oradores.map(p => p.name)
		const tiempoText =
			restante === 0
				? `${Math.trunc(tiempo / 60)}:${tiempo % 60 < 10 ? '0' + tiempo % 60 : tiempo % 60}`
				: `${Math.trunc(restante / 60)}:${restante % 60 < 10 ? '0' + restante % 60 : restante % 60}`
		return (
			<div>
				<Modal
					open={this.state.modalOpen}
					closeModal={() => this.setState({ modalOpen: false })}
					paises={paises.filter(p => (paises_oradores_name.indexOf(p.name) === -1 ? p : null))}
					sendOradoresList={this.sendOradoresList}
					idVotacion={this.state.idVotacion}
				/>
				<ModalConfirmacion
					done={() => this.setState({ modal1Open: false, idVotacion: '-1' })}
					open={this.state.modal1Open}
					closeModal={() => this.setState({ modal1Open: false })}
					setOradores={this.props.setOradores}
					socket={this.props.socket}
				/>

				<Grid container className={classes.root} direction={'row'}>
					<Grid item xs={5} className={classes.left}>
						<Grid container direction={'column'} justify={'space-between'} className={classes.container}>
							<Button variant="contained" className={classes.button} onClick={this.nuevaLista}>
								Nueva lista de oradores
							</Button>
							<Button variant="contained" className={classes.button} onClick={this.agregadoManual}>
								Agregar pais manualmente
							</Button>
							<Card className={classes.card}>
								<Typography className={classes.cardTitle}>
									{' '}
									{orador_actual || 'Para comenzar, seleccione un pais'}{' '}
								</Typography>
								<Divider />
								<Grid
									className={classes.containerTemporizador}
									justify={'center'}
									alignItems={'center'}
									container
									direction={'row'}
								>
									<Button
										variant="contained"
										className={classes.buttonTemporizador}
										fullWidth
										onClick={this.handleInit}
										color="primary"
										disabled={this.state.intervalId !== -1 || !orador_actual}
									>
										Iniciar
									</Button>
									<Button
										variant="outlined"
										className={classes.optionButtons}
										onClick={this.handlePause}
										disabled={this.state.intervalId === -1}
										color="primary"
									>
										<FontAwesomeIcon icon={faPause} style={{ paddingRight: 10 }} /> Pausar
									</Button>
									<Button
										variant="outlined"
										className={classes.optionButtons}
										onClick={this.handleRestart}
										color="primary"
									>
										<FontAwesomeIcon icon={faUndo} style={{ paddingRight: 10 }} /> Restaurar
									</Button>
									<Typography className={classes.tiempo}> {tiempoText} </Typography>
									<IconButton color="primary" onClick={() => this.restar()}>
										<FontAwesomeIcon icon={faArrowDown} />
									</IconButton>
									<IconButton color="primary" onClick={() => this.sumar()}>
										<FontAwesomeIcon icon={faArrowUp} />
									</IconButton>
								</Grid>
							</Card>
						</Grid>
					</Grid>
					<Grid item xs={7} className={classes.right}>
						<Grid container direction={'column'} justify={'space-between'} className={classes.container}>
							{/* <InputLabel htmlFor="listasOradores">Seleccionar lista de oradores</InputLabel> */}
							<TextField
								variant='outlined'
								value={listas.length? this.state.idVotacion : 'No hay listas de oradores creadas'}
								onChange={this.handleChangeSelect}
								label={'Listas de oradores'}
								disabled={!listas.length}
								select
							>
								{listas.length ? (
									listas.map(lista => {
										return (
											<MenuItem key={lista.id} value={lista.id}>
												{lista.asunto}
											</MenuItem>
										)
									})
								) : (
									['No hay listas de oradores creadas'].map(e => 
									<MenuItem key={e} value={e} >{e}</MenuItem>)
								)}
							</TextField>
							<Card className={classes.cardRight}>
								<Typography className={classes.cardTitle}> Oradores </Typography>
								<Divider />
								<Grid justify={'center'} alignItems={'center'} container direction={'row'}>
									{oradores.map(pais => (
										<Grid key={pais.name + '-gridRoot'} item xs={3}>
											<PaisItem
												key={pais.name + '-paisItem'}
												nombre={pais.name}
												spoken={pais.spoken}
												handleOnClick={this.handleOnClick}
											/>
										</Grid>
									))}
								</Grid>
							</Card>
						</Grid>
					</Grid>
				</Grid>
			</div>
		)
	}
}

const MapStateToProps = (state, props) => ({
	socket: state.connection.socket,
	paises: state.paises.list,
	oradores: state.oradores.list,
	listas: state.oradores.listas
})

export default connect(MapStateToProps, { openSocket, setOradores })(withStyles(styles)(Exposicion))
