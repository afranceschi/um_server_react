import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import Beforeunload from 'react-beforeunload'
import { withStyles } from '@material-ui/core/styles'
import {
	Drawer,
	AppBar,
	Toolbar,
	MenuList,
	MenuItem,
	ListItemText,
	Card,
	Snackbar,
	Dialog,
	DialogTitle,
	DialogContent,
	DialogContentText,
	DialogActions,
	Button,
	RadioGroup,
	Radio,
	FormControl,
	FormControlLabel
} from '@material-ui/core'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import { url } from './../../constants'
import { openSocket } from './../../actions/openSocket'
import { setTimestamp } from '../../actions/setTimestamp'
import { setPaises } from './../../actions/setPaises'
import { setAFavor } from './../../actions/setAFavor'
import { setEnContra } from './../../actions/setEnContra'
import { setNoVotaron } from './../../actions/setNoVotaron'
import { setAbstencion } from './../../actions/setAbstencion'
import { setVotacionCron } from './../../actions/setVotacionCron'
import { setIdIntervaloVotacion } from './../../actions/setIdIntervaloVotacion'
import { setOradores } from './../../actions/setOradores'
import { setListasOradores } from './../../actions/setListasOradores'
import { setCronoForm } from './../../actions/setCronoForm'
import { setCronograma } from './../../actions/setCronograma'
import { setEventos } from './../../actions/setEventos'
import { setDocuments } from './../../actions/setDocuments'
import { setAsamblea } from './../../actions/setAsamblea'
import { setVotaciones } from './../../actions/setVotaciones'

import Asistencia from './Asistencia'
import Exposicion from './Exposicion'
import Votaciones from './Votaciones'
import Mensajes from './Mensajes'
import Eventos from './Eventos'
import Cronograma from './Cronograma'
import Reglamento from './Reglamento'

import Typography from '@material-ui/core/Typography'
//import Divider from '@material-ui/core/Divider';
import logo from '../../assets/Logo_alpha.png'

const drawerWidth = 230

const styles = theme => ({
	root: {
		flexGrow: 1,
		height: '100vh'
	},
	appFrame: {
		height: '100vh',
		zIndex: 1,
		overflow: 'hidden',
		position: 'relative',
		display: 'flex',
		width: '100%'
	},
	appBar: {
		width: `calc(100% - ${drawerWidth}px)`,
		height: 50
	},
	'appBar-left': {
		marginLeft: drawerWidth
	},
	'appBar-right': {
		marginRight: drawerWidth
	},
	drawerPaper: {
		position: 'relative',
		width: drawerWidth,
		background: '#810C26',
		height: '100vh'
	},
	logoImg: {
		maxWidth: '60%',
		margin: 30
	},
	toolbar: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.default,
		padding: theme.spacing(3),
		zIndex: '-1',
		paddingTop: 64
	},
	menuItem: {
		'&:focus': {
			backgroundColor: theme.palette.primary.main,
			'& $primary, & $icon': {
				color: theme.palette.common.white
			}
		},
		'&:hover': {
			borderLeft: '10px solid #FCB319'
		},
		background: '#9F233F',
		//borderStyle: 'solid',
		//borderColor: '#BF405D',
		//borderWidth: '1px',
		borderTop: '1px solid #BF405D',
		color: 'white',
		height: '50px'
	},
	msgeCount: {
		borderRadius: '50px',
		background: '#C92A2A',
		width: '26px',
		height: '22px',
		color: 'white',
		textAlign: 'center',
		paddingTop: 4
	},
	primary: {
		fontSize: 20,
		color: 'white'
		//: 'Gotham Bold'
	},
	icon: {},
	myToolBar: {
		backgroundColor: 'white',
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px',
		justifyContent: 'space-between'
	},
	myCard: {
		boxShadow: 'rgba(137,139,141, 0.8) 0px 1px 6px',
		zIndex: '1'
	},
	myButton: {
		//: 'Gotham Medium',
		borderRadius: 30,
		width: '40%',
		textTransform: 'none'
	}
})

class Dashboard extends React.Component {
	state = {
		anchor: 'left',
		title: '',
		open: false,
		msge: '',
		today: new Date(),
		checked: '1'
	}

	asistenciaBtn = () => {
		this.context.router.history.push('/some/Path')
	}

	setTitle = title => {
		this.setState({ title })
	}

	inicializarSocket() {
		const { socket } = this.props
		if (!socket || socket.readyState === WebSocket.CLOSED) {
			const newSocket = new WebSocket(url)
			newSocket.onopen = () => this.handleOpen(newSocket)
			newSocket.onclose = () => {
				setTimeout(() => {
					this.inicializarSocket()
				}, 3000)
			}
			this.props.openSocket(newSocket)
		} else if (socket.readyState === WebSocket.OPEN) {
			// //console.log('ya estaba abierto');
		}
	}

	componentDidMount() {
		this.inicializarSocket()
		this.inicializarHoraActual()
	}

	inicializarHoraActual() {
		if (this.state.dateInterval) clearInterval(this.state.dateInterval)
		const idi = setInterval(() => {
			this.setState({ today: new Date() })
		}, 10000)
		this.setState({ dateInterval: idi })
	}

	showSnack = text => {
		this.setState({
			open: true,
			msge: text
		})
		setTimeout(() => {
			this.setState({
				open: false
			})
		}, 2500)
	}

	handleMsge = data => {
		data = JSON.parse(data)
		console.log(data)
		if (data.subject === 'get_votacion' || data.subject === 'new_voto') {
				this.props.setVotaciones(data)
		} else if (data.subject === 'get_comision') {
			if (data.comision === 0) {
				this.props.setAsamblea('')
			} else {
				this.props.setAsamblea(data.comision !== '0' ? (data.comision === '1' ? 'A' : 'B') : '')
				this.initApp()
			}
		} else if (data.subject === 'set_comision') {
			if (data.status === 'ok') {
				this.props.socket.send(JSON.stringify({ type: 'get_comision' }))
			} else if (data.status === 'err') {
				this.showSnack('Ocurrio un error al guardar los datos de la asamblea, por favor intente nuevamente')
			}
		} else if (data.subject === 'get_registered') {
			this.props.setPaises(data.list)
		} else if (data.subject === 'register') {
			const { paises } = this.props
			const p = [ ...paises ]
			const index = p.map(pais => pais.name).indexOf(data.name)
			if (index !== -1) {
				p[index].connected = data.connected
				p[index].attended = data.attended
				p[index].manual = data.manual
			}
			this.props.setPaises(p)
			var c = data.connected ? 'conectado' : 'desconectado'
			this.showSnack(data.name + ' se ha ' + c)
			setTimeout(() => {
				this.setState({
					open: false
				})
			}, 2500)
		} else if (data.subject === 'votacion') {
			//deprecated
			const { aFavor, noVotaron, enContra, abstencion } = this.props
			const af = [ ...aFavor ]
			const nv = [ ...noVotaron ]
			const ec = [ ...enContra ]
			const abs = [ ...abstencion ]
			var i = -1
			i = nv.map(pais => pais.name).indexOf(data.name)
			if (i !== -1) nv.splice(i, 1)
			i = af.indexOf(data.name)
			if (i !== -1) af.splice(i, 1)
			i = ec.indexOf(data.name)
			if (i !== -1) ec.splice(i, 1)
			i = abs.indexOf(data.name)
			if (i !== -1) abs.splice(i, 1)
			if (data.selected === 'yes') {
				af.push(data.name)
			} else if (data.selected === 'no') {
				ec.push(data.name)
			} else if (data.selected === 'abs') {
				abs.push(data.name)
			}
			this.props.setAFavor(af)
			this.props.setEnContra(ec)
			this.props.setAbstencion(abs)
			this.props.setNoVotaron(nv)
		} else if (data.subject === 'cron') {
			if (data.tiempo === 0) {
				clearInterval(this.props.idIntervalo)
			}
			this.props.setVotacionCron(data.tiempo)
		} else if (data.subject === 'oradores' && data.selected === 'yes') {
			var { oradores } = this.props
			const o = [ ...oradores ]
			o.push({ name: data.name, spoken: data.spoken })
			this.props.setOradores(o)
		} else if (data.subject === 'get_speakers') {
			this.props.setOradores(data.list)
		} else if (data.subject === 'result_actividad') {
			if (data.status === 'ok') {
				this.props.setCronoForm('id', -1)
				this.props.setCronoForm('fecha', '2018-01-01')
				this.props.setCronoForm('hora', '07:30')
				this.props.setCronoForm('titulo', '')
				this.props.setCronoForm('texto', '')
				this.props.socket.send(JSON.stringify({ type: 'get_actividades' }))
				this.props.socket.send(JSON.stringify({ type: 'get_events' }))
			} else if (data.status === 'err') {
				alert('Ha ocurrido un error en el servidor')
			}
		} else if (data.subject === 'get_actividades') {
			this.props.setCronograma(data.list)
		} else if (data.subject === 'get_events') {
			this.props.setEventos(data.list)
		} else if (data.subject === 'share_event') {
			if (data.status === 'ok') {
				this.showSnack('Evento compartido exitosamente')
				this.props.socket.send(JSON.stringify({ type: 'get_events' }))
			} else if (data.status === 'err') {
				alert('No se pudo compartir el evento debido a un error')
			}
		} else if (data.subject === 'get_documents_list') {
			this.props.setDocuments(data.list)
		} else if (data.subject === 'post_document') {
			if (data.status === 'ok') {
				this.showSnack('Documento agregado correctamente')
				this.props.socket.send(JSON.stringify({ type: 'get_documents_list' }))
			} else if (data.status === 'err') {
				this.showSnack(data.msge)
			}
		} else if (data.subject === 'delete_document') {
			if (data.status === 'ok') {
				this.showSnack('Documento eliminado correctamente')
				this.props.socket.send(JSON.stringify({ type: 'get_documents_list' }))
			} else if (data.status === 'err') {
				this.showSnack('Ocurrio un error al intentar eliminar, por favor intente de nuevo')
			}
		} else if (data.subject === 'get_speaker_lists') {
			this.props.setListasOradores(data.list)
		}
	}

	initApp = () => {
		const { socket } = this.props
		socket.send(JSON.stringify({ type: 'get_registered' }))
		socket.send(JSON.stringify({ type: 'get_speakers' }))
		socket.send(JSON.stringify({ type: 'get_votacion' }))
		socket.send(JSON.stringify({ type: 'get_speaker_lists' }))
		socket.send(
			JSON.stringify({
				type: 'get_actividades'
			})
		)
		socket.send(JSON.stringify({ type: 'get_events' }))
		this.props.socket.send(
			JSON.stringify({
				type: 'get_documents_list'
			})
		)
	}

	handleOpen = socket => {
		socket.onmessage = msge => this.handleMsge(msge.data)
		socket.onclose = () => {
			setTimeout(() => {
				this.inicializarSocket()
			}, 3000)
		}
		socket.onerror = e => console.log('error')
		socket.send(JSON.stringify({ type: 'get_comision' }))
	}

	setAsamblea = () => {
		if (this.state.asambleaAnterior) {
			const a = this.state.asambleaAnterior === 'A' ? '1' : '2'
			if (a === this.state.checked) {
				this.showSnack('Ya se encuentra en la asamblea seleccionada')
			} else {
				this.props.socket.send(
					JSON.stringify({
						type: 'set_comision',
						comision: this.state.checked
					})
				)
			}
		} else {
			this.props.socket.send(
				JSON.stringify({
					type: 'set_comision',
					comision: this.state.checked
				})
			)
		}
	}

	handleChange = event => {
		this.setState({ checked: event.target.value })
	}

	modifAsamblea = () => {
		this.setState({ asambleaAnterior: this.props.asamblea })
		this.props.setAsamblea('')
	}

	cancelModifAsamblea = () => {
		this.props.setAsamblea(this.state.asambleaAnterior)
		this.setState({ asambleaAnterior: undefined })
	}

	render() {
		const { classes, socket, asamblea } = this.props
		const { anchor, today } = this.state
		const dias = [ 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado' ]
		const meses = [
			'Enero',
			'Febrero',
			'Marzo',
			'Abril',
			'Mayo',
			'Junio',
			'Julio',
			'Agosto',
			'Septiembre',
			'Octubre',
			'Noviembre',
			'Diciembre'
		]
		const drawer = (
			<Drawer
				variant="permanent"
				classes={{
					paper: classes.drawerPaper
				}}
				style={{ boxShadow: 'rgba(137,139,141, 0.8)4px 0px 6px 0px' }}
				anchor={anchor}
			>
				<div className={classes.toolbar}>
					<center>
						<img alt="Logo" className={classes.logoImg} src={logo} />{' '}
					</center>
				</div>
				<MenuList>
					<Link to={`/`} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="ASISTENCIA"
							/>
						</MenuItem>
					</Link>
					<Link to={`/exposicion`} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="EXPOSICION"
							/>
						</MenuItem>
					</Link>
					{/* <Link to={`/mensajes`} style={{ textDecoration: 'none' }}>
                        <MenuItem className={classes.menuItem}>
                            <ListItemText classes={{ primary: classes.primary }} style={{paddingLeft: 20}} inset primary="MENSAJES" />
                            <Typography
                                align={'center'}
                                className={classes.msgeCount}>
                                5
                            </Typography>
                        </MenuItem>
                    </Link> */}
					<Link to={`/votacion`} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="VOTACION"
							/>
						</MenuItem>
					</Link>
					<Link to={'/cronograma'} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="CRONOGRAMA"
							/>
						</MenuItem>
					</Link>
					<Link to={'/reglamento'} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="REGLAMENTO"
							/>
						</MenuItem>
					</Link>
					<Link to={'/eventos'} style={{ textDecoration: 'none' }}>
						<MenuItem className={classes.menuItem} style={{ borderBottom: '1px solid #BF405D' }}>
							<ListItemText
								classes={{ primary: classes.primary }}
								style={{ paddingLeft: 20 }}
								inset
								primary="EVENTOS"
							/>
						</MenuItem>
					</Link>
				</MenuList>
			</Drawer>
		)

		let before = null
		let after = null
		if (anchor === 'left') {
			before = drawer
		} else {
			after = drawer
		}
		return (
			<Router>
				<div className={classes.root}>
					<div className={classes.appFrame}>
						<Dialog
							open={socket ? socket.readyState === WebSocket.OPEN ? false : false : false}
							open={socket ? socket.readyState === WebSocket.OPEN && asamblea ? false : true : true}
							open={socket ? socket.readyState === WebSocket.OPEN && asamblea ? false : true : true}
							onClose={this.handleClose}
							aria-labelledby="alert-dialog-title"
							aria-describedby="alert-dialog-description"
						>
							{socket ? socket.readyState === WebSocket.OPEN ? !asamblea ? (
								<div>
									<DialogTitle id="alert-dialog-title">
										Defina la asamblea en la que se encuentra
									</DialogTitle>
									<DialogContent>
										<FormControl component="fieldset" className={classes.formControl}>
											<RadioGroup
												name="asamblea"
												value={this.state.checked}
												onChange={this.handleChange}
											>
												<FormControlLabel value={'1'} control={<Radio />} label="Asamblea A" />
												<FormControlLabel value={'2'} control={<Radio />} label="Asamblea B" />
											</RadioGroup>
										</FormControl>
									</DialogContent>
									<DialogActions>
										{this.state.asambleaAnterior ? (
											<Button
												onClick={() => this.cancelModifAsamblea()}
												className={classes.myButton}
												color="primary"
											>
												Cancelar
											</Button>
										) : null}
										<Button
											onClick={() => this.setAsamblea()}
											className={classes.myButton}
											variant="contained"
											color="primary"
										>
											Aceptar
										</Button>
									</DialogActions>
								</div>
							) : (
								''
							) : (
								<div>
									<DialogTitle id="alert-dialog-title">
										{'Conexión con el servidor perdida'}
									</DialogTitle>
									<DialogContent>
										<DialogContentText id="alert-dialog-description">
											Reintentando...
										</DialogContentText>
									</DialogContent>
								</div>
							) : (
								<div>
									<DialogTitle id="alert-dialog-title">
										{'Conexión con el servidor perdida'}
									</DialogTitle>
									<DialogContent>
										<DialogContentText id="alert-dialog-description">
											Conectando...
										</DialogContentText>
									</DialogContent>
								</div>
							)}
						</Dialog>
						<Beforeunload onBeforeunload={() => 'prueba'} />
						<Snackbar
							anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
							open={this.state.open}
							onClose={() => this.setState({ open: false })}
							ContentProps={{
								'aria-describedby': 'message-id'
							}}
							autoHideDuration={6000}
							message={<span id="message-id">{this.state.msge}</span>}
						/>
						<AppBar position="absolute" className={classNames(classes.appBar, classes[`appBar-${anchor}`])}>
							<Card className={classes.myCard}>
								<Toolbar className={classes.myToolBar}>
									<Typography variant="h6" color="primary" noWrap>
										{`Asamblea ${asamblea} `}
										{/* <Edit style={{fontSize: 20}} color='primary' onClick={() => this.modifAsamblea()}/> */}
									</Typography>
									<Typography variant="h6" color="primary" noWrap>
										{`${dias[today.getDay()]} ${today.getDate()} de ${meses[today.getMonth()]}, 
                                    ${today.getHours() < 10
										? '0' + today.getHours()
										: today.getHours()}:${today.getMinutes() < 10
											? '0' + today.getMinutes()
											: today.getMinutes()}`}
									</Typography>
								</Toolbar>
							</Card>
						</AppBar>
						{before}
						<main className={classes.content}>
							{/* <div className={classes.toolbar} /> */}
							<Route exact path="/" render={() => <Asistencia />} />
							<Route path="/votacion" render={() => <Votaciones />} />
							<Route path="/exposicion" render={() => <Exposicion />} />
							<Route path="/mensajes" render={() => <Mensajes />} />
							<Route path="/eventos" render={() => <Eventos />} />
							<Route path="/cronograma" render={() => <Cronograma dias={dias} meses={meses} />} />
							<Route path={'/reglamento'} render={() => <Reglamento showSnack={this.showSnack} />} />
						</main>
						{after}
					</div>
				</div>
			</Router>
		)
	}
}

const MapStateToProps = (state, props) => ({
	socket: state.connection.socket,
	paises: state.paises.list,
	aFavor: state.votacion.aFavor,
	enContra: state.votacion.enContra,
	abstencion: state.votacion.abs,
	noVotaron: state.votacion.noVotaron,
	oradores: state.oradores.list,
	asamblea: state.asamblea.value,
	idIntervalo: state.votacion.idIntervalo,
	timestamp: state.votacion.timestamp
})

const MapDispatchToProps = {
	openSocket,
	setPaises,
	setAFavor,
	setEnContra,
	setNoVotaron,
	setAbstencion,
	setVotacionCron,
	setOradores,
	setCronoForm,
	setCronograma,
	setEventos,
	setDocuments,
	setAsamblea,
	setIdIntervaloVotacion,
	setListasOradores,
	setTimestamp,
	setVotaciones,
}

export default connect(MapStateToProps, MapDispatchToProps)(withStyles(styles)(Dashboard))
