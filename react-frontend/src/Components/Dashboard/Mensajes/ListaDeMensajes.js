import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, CardHeader, List, Typography, CardContent, Divider, ListItemText, ListItem } from '@material-ui/core';
import { Chat } from '@material-ui/icons'

class ListaDeMensajes extends Component {
    render() {
        return (
            <Card className={this.props.classProp}>
                <CardHeader title={<span>
                    <Chat color='primary' />Mensaje
                </span>}
                />
                <Divider />
                <CardContent>
                    <Typography variant={'display1'}>Remitente aqui</Typography>
                    <List component={'nav'}>
                        {/* aca va el map al arreglo de mensajes que arma cada list item*/}
                        <ListItem button onClick={() => this.props.mensajeClick('idmsje')}>
                            <ListItemText primary="ASUNTO MENSAJE 1" />
                        </ListItem>
                    </List>
                </CardContent>
            </Card>
        )
    }
}

ListaDeMensajes.propTypes = {
    classProp: PropTypes.string.isRequired,
    mensajeClick: PropTypes.func.isRequired,
}

export default ListaDeMensajes