import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Typography, Card, CardHeader, CardContent, TextField,
    Button, List, ListItem, ListItemText, Divider, CardActions } from '@material-ui/core';
import { Email, Send } from '@material-ui/icons'

import Mensaje from './Mensaje'
import ListaDeMensajes from './ListaDeMensajes'

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
        padding: '5px',
    },
    left: {
        width: '33%',
        float: 'left',
        padding: '8px',
    },
    right: {
        width: '66%',
        float: 'right',
        padding: '8px',
    },
    msgeCount: {
        borderRadius: '50px',
        background: '#9f233f',
        width: '20px',
        height: '20px',
        color: 'white',
    },
    topCard: {
        marginBottom: '4px',
    },
    bottomCard: {
        marginTop: '4px',
    },
    listContainer: {
        maxHeight: '600px',
    },
}) 

class Mensajes extends Component {
    constructor() {
        super();
        this.state = {
            verMensaje: false,
            remitenteSeleccionado: null,
            mensajes: [],
            mensajeSeleccionado: {},
        }
    }
    handleResponder = () => {

    }

    handleOnBack = () => {
        this.setState({verMensaje: false});
    }

    mensajeClick = nroMsje => {
        this.setState({verMensaje: true}); 
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container alignItems={'stretch'}
                    direction={'row'} justify={'center'}>
                    <Grid item xs={4} className={classes.left}>
                        <Grid container direction={'column'} justify={'center'}
                            alignItems={'stretch'}>
                            <Card className={classes.topCard}>
                                <CardHeader title={<Typography>AUTORIDADES</Typography>} />
                                <Divider/>
                                <CardContent>
                                    <List component="nav">
                                        <ListItem button>
                                            <ListItemText primary="ITEM1" />
                                            <Typography 
                                                align={'center'}
                                                className={classes.msgeCount}>
                                                3
                                            </Typography>
                                        </ListItem>
                                        <ListItem button>
                                            <ListItemText primary="ITEM2" />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </Card>
                            <Card className={classes.bottomCard}>
                                <CardHeader title={<Typography>Delegacion</Typography>} />
                                <Divider/>                                
                                <CardContent className={classes.listContainer}>
                                    <List component="nav" >
                                        <ListItem button>
                                            <ListItemText primary="PAIS1" />
                                            <Typography 
                                                align={'center'}
                                                className={classes.msgeCount}>
                                                45
                                            </Typography>
                                        </ListItem>
                                        <ListItem button>
                                            <ListItemText primary="PAIS2" />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                    <Grid item xs={8} className={classes.right}>
                        <Grid container direction={'column'} justify={'center'}
                            alignItems={'stretch'}>
                            {this.state.verMensaje ?
                                <Mensaje classProp={classes.topCard}
                                    handleResponder={this.handleResponder}
                                    handleOnBack={this.handleOnBack}
                                />
                                :
                                <ListaDeMensajes classProp={classes.topCard}
                                    mensajeClick={this.mensajeClick} 
                                />
                            }
                            <Card className={classes.bottomCard}>
                                <CardHeader title={<span><Email color='primary'/>Nuevo mensaje</span>}/>
                                <Divider />
                                <CardContent>
                                    <TextField
                                        label='Para'
                                        margin='normal'
                                        fullWidth
                                    />
                                    <TextField
                                        label='Asunto'
                                        margin='normal'
                                        fullWidth
                                    />
                                    <TextField
                                        label="Mensaje"
                                        multiline
                                        rows="4"
                                        margin="normal"
                                        fullWidth
                                    />
                                </CardContent>
                                <CardActions>
                                    <div style={{ width: '100%' }}>
                                        <Button variant={'contained'} color={'primary'}
                                            onClick={() => this.props.handleOnClick()}
                                        >
                                            {'Enviar '}<Send />
                                        </Button>
                                    </div>
                                </CardActions>
                            </Card>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withStyles(styles)(Mensajes)