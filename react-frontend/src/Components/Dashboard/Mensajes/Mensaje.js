import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, CardHeader, Divider, CardContent,
    Button, CardActions, Typography } from '@material-ui/core';
import  {ArrowBack} from '@material-ui/icons/'

class Mensaje extends Component {
    render() {
        return(
            <Card className={this.props.classProp}>
                <CardHeader title={<span>
                    <Button size="small" onClick={this.props.handleOnBack}>
                        <ArrowBack />
                    </Button>Mensaje</span>} 
                />
                <Divider />
                <CardContent>
                    <Typography variant={'display1'}>from</Typography>
                    <Typography variant={'display1'}>title</Typography>
                    <Typography variant={'headline'}>content</Typography>                                        
                </CardContent>
                <CardActions style={{width:'100%'}}>
                    <div style={{width:'100%'}}>
                        <Button variant={'contained'} color={'primary'} style={{float:'right'}}
                            onClick={() => this.props.handleOnClick()}
                        >
                            Responder
                        </Button>
                    </div>
                </CardActions>
            </Card>
        )
    }
}

Mensaje.propTypes = {
    handleOnClick: PropTypes.func.isRequired,
    classProp: PropTypes.string.isRequired, 
    handleOnBack: PropTypes.func.isRequired,
}

export default Mensaje