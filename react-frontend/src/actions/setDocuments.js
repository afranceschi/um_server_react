import { createAction } from 'redux-actions'
import { SET_DOCUMENTOS } from './../constants'

export const setDocuments = createAction(SET_DOCUMENTOS, list => list);