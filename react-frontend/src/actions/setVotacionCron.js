import { createAction } from 'redux-actions'
import { SET_VOTACION_CRON } from './../constants'

export const setVotacionCron = createAction(SET_VOTACION_CRON, time => time);