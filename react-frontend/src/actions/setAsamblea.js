import { createAction } from 'redux-actions'
import { SET_ASAMBLEA } from './../constants'

export const setAsamblea = createAction(SET_ASAMBLEA, asamblea => asamblea);