import { createAction } from "redux-actions"
import { SET_PAISES } from './../constants/'

export const setPaises = createAction(SET_PAISES, paises => paises);