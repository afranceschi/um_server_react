import { createAction } from 'redux-actions'
import { SET_CRONOGRAMA } from './../constants'

export const setCronograma = createAction(SET_CRONOGRAMA, list => list);