import { createAction } from "redux-actions"
import { SET_NOVOTARON } from './../constants/'

export const setNoVotaron = createAction(SET_NOVOTARON, noVotaron => noVotaron);