import { createAction } from "redux-actions"
import { SET_ABSTENCION } from './../constants/'

export const setAbstencion = createAction(SET_ABSTENCION, abs => abs);