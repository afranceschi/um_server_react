import { createAction } from 'redux-actions'
import { SET_TIMESTAMP } from '../constants'

export const setTimestamp = createAction(SET_TIMESTAMP, ts => ts)