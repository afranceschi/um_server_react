import { createAction } from 'redux-actions'
import { SET_ORADORES } from './../constants'

export const setOradores = createAction(SET_ORADORES, oradores => oradores);