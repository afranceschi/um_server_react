import { createAction } from 'redux-actions'
import { SET_CRONO_FORM }from './../constants'

export const setCronoForm = createAction(SET_CRONO_FORM, (name, text) => ({name, text}));