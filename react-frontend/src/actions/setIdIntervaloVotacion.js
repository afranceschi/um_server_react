import { createAction } from 'redux-actions'
import { SET_IDINTERVAL_VOTACION } from './../constants'

export const setIdIntervaloVotacion = createAction(SET_IDINTERVAL_VOTACION, time => time);