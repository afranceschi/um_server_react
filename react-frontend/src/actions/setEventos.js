import { createAction } from 'redux-actions'
import { SET_EVENTOS } from './../constants'

export const setEventos = createAction(SET_EVENTOS, eventos => eventos);