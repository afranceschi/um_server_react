import { createAction } from 'redux-actions'
import { OPEN_SOCKET } from './../constants'

export const openSocket = createAction(OPEN_SOCKET, socket => socket);