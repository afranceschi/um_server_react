import { createAction } from 'redux-actions'
import { SET_LISTAS_ORADORES } from './../constants'

export const setListasOradores = createAction(SET_LISTAS_ORADORES, listas => listas);