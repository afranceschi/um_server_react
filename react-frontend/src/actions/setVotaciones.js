import { createAction } from 'redux-actions'
import { SET_VOTACIONES } from '../constants'

export const setVotaciones = createAction(SET_VOTACIONES, votaciones => votaciones)