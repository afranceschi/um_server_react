import { createAction } from 'redux-actions'
import { SET_AFAVOR } from './../constants'

export const setAFavor = createAction(SET_AFAVOR, aFavor => aFavor);