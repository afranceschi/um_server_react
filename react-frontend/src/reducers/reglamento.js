import { handleActions } from 'redux-actions'
import { SET_DOCUMENTOS } from './../constants'

export const reglamento = handleActions({
    [SET_DOCUMENTOS]: (state, action) => {
        return {...state, list: action.payload}
    },
}, {list: []});