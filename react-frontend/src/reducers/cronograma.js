import { handleActions } from 'redux-actions'
import { SET_CRONO_FORM, SET_CRONOGRAMA } from './../constants'

export const cronograma = handleActions({
    [SET_CRONO_FORM]: (state, action) => {
        //console.log(action.payload)
        return {...state, form: {...state.form, [action.payload.name]: action.payload.text}}
    },
    [SET_CRONOGRAMA]: (state, action) => {
        return {...state, cron: action.payload}
    },
}, {form: {id: -1, fecha: '2018-01-01', titulo: '', texto: '', hora: '07:30'}, cron: []});