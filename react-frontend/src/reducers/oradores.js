import { handleActions } from 'redux-actions'
import { SET_ORADORES, SET_LISTAS_ORADORES } from './../constants'

export const oradores = handleActions({
    [SET_ORADORES]: (state, action) => {
        return {...state, list: action.payload}
    },
    [SET_LISTAS_ORADORES]: (state, action) => {
        return {...state, listas: action.payload}
    }
}, {list: [], listas: []});