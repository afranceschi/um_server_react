import { handleActions } from 'redux-actions'
import { SET_ASAMBLEA } from './../constants'

export const asamblea = handleActions({
    [SET_ASAMBLEA]: (state, action) => {
        return {...state, value: action.payload}
    }
}, {value: ''});