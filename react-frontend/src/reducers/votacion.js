import { handleActions } from 'redux-actions'
import { SET_AFAVOR, SET_NOVOTARON, SET_ABSTENCION, SET_ENCONTRA, SET_VOTACION_CRON, SET_IDINTERVAL_VOTACION, SET_TIMESTAMP, SET_VOTACIONES } from './../constants'

export const votacion = handleActions({
    [SET_AFAVOR]: (state, action) => {
        return {...state, aFavor: action.payload}
    },
    [SET_NOVOTARON]: (state, action) => {
        return {...state, noVotaron: action.payload}
    },
    [SET_ABSTENCION]: (state, action) => {
        return {...state, abs: action.payload}
    },
    [SET_ENCONTRA]: (state, action) => {
        return {...state, enContra: action.payload}
    },
    [SET_VOTACION_CRON]: (state, action) => {
        return {...state, time: action.payload}
    },
    [SET_IDINTERVAL_VOTACION]: (state, action) => {
        return {...state, idIntervalo: action.payload}
    },
    [SET_TIMESTAMP]: (state, a) => {
        return {...state, timestamp: a.payload}
    },
    [SET_VOTACIONES]: (state, a) => {
        const {noVotaron, aFavor, enContra, abstencion } = a.payload
        return {...state, noVotaron, aFavor, enContra, abs: abstencion }
    }
}, {aFavor: [], noVotaron: [], abs: [], enContra:[], time: 30, idIntervalo: null, timestamp: 0});