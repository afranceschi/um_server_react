import { handleActions } from 'redux-actions'
import { SET_EVENTOS } from './../constants'

export const eventos = handleActions({
    [SET_EVENTOS]: (state, action) => {
        return {...state, list: action.payload}
    }
}, {list: []});