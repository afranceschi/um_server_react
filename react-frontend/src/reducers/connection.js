import { handleActions } from 'redux-actions'
import { OPEN_SOCKET } from './../constants'

export const connection = handleActions({
    [OPEN_SOCKET]: (state, action) => {
        return {...state, socket: action.payload}
    }
}, {socket: null});