import { combineReducers } from 'redux';
import { connection } from './connection'
import { eventos } from './eventos'
import { votacion } from './votacion'
import { oradores } from './oradores'
import { paises } from './paises'
import { cronograma } from './cronograma'
import { reglamento } from './reglamento'
import { asamblea } from './asamblea'

export default combineReducers({
    connection,
    eventos,
    paises,
    votacion,
    oradores,
    cronograma,
    reglamento,
    asamblea,
});