import { handleActions } from 'redux-actions'
import { SET_PAISES } from './../constants'

export const paises = handleActions({
    [SET_PAISES]: (state, action) => {
        return {...state, list: action.payload}
    },
}, {list: []});