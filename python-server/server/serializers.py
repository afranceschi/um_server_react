from rest_framework import serializers
from .models import *

# serializers aca


class DiaCronogramaSerializer(serializers.ModelSerializer):

    class Meta:
        model = DiaCronograma
        fields = ()


class ActividadCronogramaSerializer(serializers.ModelSerializer):

    fecha = DiaCronogramaSerializer()

    class Meta:
        model = ActividadCronograma
        fields = ('titulo', 'hora', 'fecha', 'texto', 'id')

class ListaOradoresSerializer(serializers.ModelSerializer):

    class Meta:
        model = ListaOradores
        fields = ('id', 'asunto', 'hora')