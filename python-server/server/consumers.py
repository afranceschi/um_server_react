from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from .models import *
import json
import base64
import time
import threading
from datetime import datetime
from .serializers import *
from os import listdir, path, remove, mkdir
from os.path import isfile, join, exists
from random import randint, random

from django.conf import settings # correct way
BASE_DIR = settings.BASE_DIR

#IPHOST = '192.168.0.6'
IPHOST = '10.0.0.5'

MAX_CANT_TERMINALES = 5

# Para levantar Redis, correr
# docker run -p 6379:6379 -d redis:2.8

# Channel name del Terminal

list_channel_name = [] # Lista de channel names de terminales
TERMINAL_GROUP_NAME = "group_terminal"

channel_name = None


last_poll = {
    'aFavor': [],
    'enContra': [],
    'abstencion': [],
    'noVotaron': []
}


last_speakers = []



class Client():

    def __init__(self, pais, channel_name, connected, attended, manual, observadora):
        self.pais = pais
        self.channel_name = channel_name
        self.connected = connected
        self.attended = attended
        self.manual = manual
        self.observadora = observadora




# lista de objetos Client
registered_clients = []
paises = Pais_Sede.objects.all().order_by('pais')
if paises.count() != 0:
    nombres = [x.pais for x in paises]
    paises = Paises.objects.filter(pais__in=nombres).order_by('pais')
else:
    paises = Paises.objects.all().order_by('pais')
    
for i in paises:
    c = Client(i.pais, None, 0, 0, 0, i.observadora)
    registered_clients.append(c)



# Ultimo evento
lastEvento = None
# JSON Ultimo evento
package = None


# Obtener comision en la que está corriento
try:
    comision = Comision.objects.all()[0]
except Exception:
    comision = None


class ServerConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        global list_channel_name
        global TERMINAL_GROUP_NAME
        if (self.scope['url_route']['kwargs']['rol'] == 'terminal'):

            await self.channel_layer.group_add(
                TERMINAL_GROUP_NAME,
                self.channel_name
            )
        
        await self.accept()

    async def disconnect(self, close_code):
        global TERMINAL_GROUP_NAME
        if (self.scope['url_route']['kwargs']['rol'] == 'terminal'):

            await self.channel_layer.group_discard(
                TERMINAL_GROUP_NAME,
                self.channel_name
            )
            return 0
            

        pais = None
        await self.send(text_data=json.dumps({'subject': 'disconnect'}))
        for i in registered_clients:
            if (i.channel_name == self.channel_name):
                pais = i.pais
                i.connected = 0
                break

        try:
            if pais is not None:
                await self.channel_layer.group_send(
                    TERMINAL_GROUP_NAME,
                    {'type': 'notificate_terminal', 'name':pais, 'subject': 'register', 'connected': 0}
                )

        except Exception:
            pass


    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        tipo = text_data_json['type']
        if (tipo.startswith('broadcast')):

            global package
            global lastEvento
            evento = Evento()
            evento.mensaje = text_data_json['question']
            evento.hora = time.time()
            evento.tipo = text_data_json['subject']
            evento.save()

            lastEvento = evento
            package = text_data_json
            package['id'] = evento.id

            if (text_data_json['subject'] == 'oradores'):
                ListaOradores.objects.create(id = evento.id, asunto= text_data_json['question'])
                await self.channel_layer.send(self.channel_name, {'type': 'get_speaker_lists'})

                # broadcast

                for i in registered_clients:
                    if (i.connected and not i.manual):
                        await self.channel_layer.send(
                            i.channel_name,
                            text_data_json
                        )


            elif (text_data_json['subject'] == 'votacion'):
                Votacion.objects.create(idVotacion = evento.id, asunto = text_data_json['question'])

                #broadcast

                for i in registered_clients:
                    if (i.connected and not i.manual and not i.observadora):
                        await self.channel_layer.send(
                            i.channel_name,
                            text_data_json
                        )

        else:
            await self.channel_layer.send(self.channel_name, text_data_json)


    # Handlers para cada Method del protocolo

    async def register(self, event):
        global comision
        token = event['token']
        try:
            if comision:

                if (int(comision.numero) == 1):
                    if int(token)>500:
                        await self.send(text_data=json.dumps({
                            'subject': 'register',
                            'response' : 'err'
                        }))
                        return 0
                elif (int(comision.numero) == 2):
                    if int(token)<500:
                        await self.send(text_data=json.dumps({
                            'subject': 'register',
                            'response' : 'err'
                        }))
                        return 0

                user = Usuario.objects.get(token=token)
                pais = user.pais
                found = 0

                i = len(registered_clients)//2
                base = 0
                tope = len(registered_clients)-1

                while registered_clients[i].pais != pais and (base <= tope):
                    if registered_clients[i].pais > pais:
                        tope = i - 1
                    else:
                        base = i + 1
                    i = base + (tope - base) // 2

                found = (registered_clients[i].pais == pais)

                channel_layer = get_channel_layer()
                code = 'err'
                if found:
                    if registered_clients[i].channel_name:
                        await channel_layer.send(str(registered_clients[i].channel_name), {'type': 'disconnect'})
                    
                        
                    registered_clients[i].channel_name = self.channel_name
                    registered_clients[i].connected = 1
                    registered_clients[i].attended = 1
                    registered_clients[i].manual = 0

                    code = 'ok'
                    observadora = 1 if registered_clients[i].observadora else 0
                        
                    
                    try:
                        await self.channel_layer.group_send(
                            TERMINAL_GROUP_NAME,
                            {'type': 'notificate_terminal', 
                                'name':pais, 
                                'subject': 'register', 
                                'connected': 1, 
                                'attended':1, 
                                'manual':0, 
                                'observadora': observadora}
                        )

                       
                    except Exception:
                        pass

                await self.send(text_data=json.dumps({
                    'subject': 'register',
                    'response' : code
                }))
                
                
            else:
                await self.send(text_data=json.dumps({
                'subject': 'register',
                'response' : 'err'
            }))
        except Usuario.DoesNotExist:
            await self.send(text_data=json.dumps({
                'subject': 'register',
                'response' : 'err'
            }))


    async def broadcast_ask(self, event):
        global lastEvento, last_poll, last_speakers
        evento = lastEvento

        if event['subject'] == 'oradores':
            last_poll = []
        elif event['subject'] == 'votacion':

            last_poll = {
                'aFavor': [[x.pais, 0] for x in registered_clients if x.attended],
                'enContra': [[x.pais, 0] for x in registered_clients if x.attended],
                'abstencion': [[x.pais, 0] for x in registered_clients if x.attended],
                'noVotaron': [[x.pais, 1] for x in registered_clients if x.attended]
            }


        data = json.dumps({
            'id': evento.id,
            'subject': event['subject'],
            'question': event['question'],
            'options': event['options'],
        })

        await self.send(text_data = data)

    async def answer_ask(self, event):
        global lastEvento
        global package
        
        if event['id'] == -1:
           
            ahora = time.time()
            if lastEvento != None:
                if (ahora-lastEvento.hora < 30):
                    await self.send(text_data = json.dumps(package))
                else:
                    await self.send(text_data = json.dumps({'response': 'no'}))
            
            return 0
        elif event['id'] < -1:
            return 0

        selected = event['selected']
        if lastEvento.id != event['id']:
            evento = Evento.objects.get(id = event['id'])
        else:
            evento = lastEvento

        # si la diferencia es menor a 30 segundos, ok, sino return expired
        global list_channel_name
        global TERMINAL_GROUP_NAME
        ahora = time.time()

        if (ahora-evento.hora> 30):

            if event['subject'] == 'votacion':
                cartel = "Votación finalizada"
            elif event['subject'] == 'oradores':
                cartel = "Lista de oradores cerrada"
            else:
                cartel = "Evento finalizado"

            await self.send(text_data=json.dumps({
                    'id': -2,
                    'subject': "votacion",
                    'question': cartel,
                    'options': [{'text': 'Aceptar', 'color': '#4bbc75', 'value': 'yes'}],
                
            }))
            return 0
        subject = event['subject']
        pais = None
        for i in registered_clients:
            if i.channel_name == self.channel_name:
                pais = i.pais
                break


        if (subject == 'oradores'):
            global last_speakers

            if (selected == 'yes'):
                lista = ListaOradores.objects.get(id=event['id'])
                DelegacionOradora.objects.create(pais=pais, lista=lista)
                
                last_speakers.append(pais)


                try:
                    await self.channel_layer.group_send(
                        TERMINAL_GROUP_NAME,
                        {'type': 'notificate_terminal', 
                            'name':pais,
                            'selected':selected,
                            'subject': subject,
                            'spoken':0,
                            'lastSpeakers': last_speakers}
                    )

                except Exception as e:
                    print(e, flush=True)


        elif (subject == 'votacion'):
            

            global last_poll

            v = Votacion.objects.get(idVotacion=event['id'])
            try:
                votoAnterior = Voto.objects.get(idVotacion=v, pais=pais)
            except Voto.DoesNotExist:
                votoAnterior = None
            if votoAnterior:
                return 0
            else:
                Voto.objects.create(idVotacion=v, pais=pais, voto=selected)

            try:
                # buscar en la estucrtura de votos en donde está
                # y setear el voto
                index = None
                for idx, country in enumerate(last_poll['noVotaron']):
                    if country[0] == pais:
                        index = idx
                        break

                if index is not None:
                    last_poll['noVotaron'][index][1] = 0


                    if selected == 'yes':
                        last_poll['aFavor'][index][1] = 1
                    elif selected == 'no':
                        last_poll['enContra'][index][1] = 1
                    elif selected == 'abs':
                        last_poll['abstencion'][index][1] = 1

                
                await self.channel_layer.group_send(
                    TERMINAL_GROUP_NAME,
                    {'type': 'notificate_terminal', 
                            'aFavor':last_poll['aFavor'],
                            'noVotaron':last_poll['noVotaron'],
                            'enContra':last_poll['enContra'],
                            'abstencion':last_poll['abstencion'],
                            'subject':'new_voto'}
                )

            except Exception as e:
                raise

        await self.send(text_data=json.dumps({
            'response' : 'ok'
        }))




    async def get_flag(self, event):
        pais = None
        for i in registered_clients:
            if i.channel_name == self.channel_name:
                pais = i.pais
                break

        p = Paises.objects.get(pais=pais)
        try:
            with open(f'{BASE_DIR}/banderas/' + p.codigo.lower() + ".png", "rb") as image_file:
                encoded_flag = base64.b64encode(image_file.read())
    
        except Exception:
            with open(f'{BASE_DIR}/banderas/placeholder.png', "rb") as image_file:
                encoded_flag = base64.b64encode(image_file.read())

        

        await self.send(text_data=json.dumps({
            'flag' : "data:image/png;base64," +  encoded_flag.decode('utf-8'),
            'subject': 'get_flag'
        }))

    async def notificate_terminal(self, event):

        global last_poll, last_speakers, TERMINAL_GROUP_NAME

        selected = None if 'selected' not in event else event['selected']

        connected = None if 'connected' not in event else event['connected']

        name = None if 'name' not in event else event['name']

        lista = None if 'list' not in event else event['list']

        spoken = 0 if 'spoken' not in event else event['spoken']

        attended = 0 if 'attended' not in event else event['attended']

        manual = 0 if 'manual' not in event else event['manual']

        ultima_votacion = None if 'lastPoll' not in event else event['lastPoll']

        ultimos_oradores = None if 'lastSpeakers' not in event else event['lastSpeakers']

        a_favor = None if 'aFavor' not in event else event['aFavor']
        no_votaron = None if 'noVotaron' not in event else event['noVotaron']
        en_contra = None if 'enContra' not in event else event['enContra']
        abstencion = None if 'abstencion' not in event else event['abstencion']
        
        #time.sleep(0.1 + randint(0, 10))


        resp = {
            'type': event['type'],
            'subject': event['subject'],
            'name': name,
            'selected': selected,
            'connected': connected,
            'list': lista,
            'spoken': spoken,
            'attended': attended,
            'manual': manual,
            'lastPoll': ultima_votacion,
            'lastSpeakers': ultimos_oradores,
            'aFavor': a_favor,
            'noVotaron': no_votaron,
            'enContra': en_contra,
            'abstencion': abstencion,

        }

        await self.send(text_data=json.dumps(resp))


    async def get_registered(self, event):

        connected = []
        for i in registered_clients:
            connected.append({'name': i.pais, 'connected': i.connected, 'attended': i.attended, 'manual': i.manual, 'observadora': i.observadora})

        global list_channel_name
        global TERMINAL_GROUP_NAME

        try:
            #channel_name = Cliente.objects.get(descripcion='moderador')
            await self.channel_layer.group_send(
                TERMINAL_GROUP_NAME,
                {'type': 'notificate_terminal', 'subject': 'get_registered', 'list': connected}
            )

        except Exception as e:
            pass


    async def get_current_event(self, event):
        
        #
        #   BIENVENIDA
        #

        await self.send(text_data=json.dumps({
            'id': -1,
            'subject': "votacion",
            'question': "Al aceptar, dará el presente a Mesa de Presidencia. No cierre la app en ningún momento",
            'options': [{'text': 'Aceptar', 'color': '#4bbc75', 'value': 'yes'}],
        }))

        

    async def get_time(self, event):
        global lastEvento, list_channel_name, last_poll, registered_clients
        global TERMINAL_GROUP_NAME
        ahora = time.time()
        if (lastEvento):
            tiempo = int(30 - (ahora-lastEvento.hora))
            if tiempo < 0:
                tiempo = 0
            await self.send(text_data = json.dumps({'subject': 'cron', 'tiempo': tiempo}))

            if tiempo == 0:
                if lastEvento.tipo == 'votacion':

                    await self.channel_layer.group_send(
                        TERMINAL_GROUP_NAME,
                        {'type': 'get_votacion'}
                    )



    async def get_events(self, event):
        votaciones = Votacion.objects.all().order_by('-idVotacion')
        l = []
        for v in votaciones:
            resultado = Voto.objects.filter(idVotacion = v)
            t = resultado.count()
            if t != 0:
                f = (resultado.filter(voto = 'yes').count()*100)/t
                c = (resultado.filter(voto = 'no').count()*100)/t
                a = (resultado.filter(voto = 'abs').count()*100)/t
                l.append({'asunto': v.asunto, 'aFavor': "%.2f" % f, 'enContra': "%.2f" % c, 'abs': "%.2f" % a, 'resultado': 'resultado', 'id': v.id, 'mostrar': v.mostrar})
        data = json.dumps(l)
        await self.send(text_data = json.dumps({'subject': 'get_events', 'list': l}))

    async def get_speaker_lists(self, event):
        listas = ListaOradores.objects.all().order_by('hora')
        serializer = ListaOradoresSerializer(listas, many=True)
        await self.send(text_data = json.dumps({'list': serializer.data, 'subject':'get_speaker_lists'}))


    async def get_speakers(self, event):
        try:
            lista = ListaOradores.objects.get(id=event['id'])
        except Exception as e:
            return 0
        oradores = DelegacionOradora.objects.filter(lista=lista)
        l = []
        for i in oradores:
            l.append({'name':i.pais, 'spoken':i.hablo})
        await self.send(text_data = json.dumps({'list':l, 'subject':'get_speakers'}))


    async def set_spoken(self, event):
        if event['name'] != "":
            #try:
            try:
                lista = ListaOradores.objects.get(id=event['id'])
            except Exception:
                return 0
            d = DelegacionOradora.objects.get(lista=lista, pais=event['name'])
            d.hablo = 1
            d.save()
            await self.send(text_data=json.dumps({'response':'ok'}))
            """
            except Exception:
                await self.send(text_data=json.dumps({'response': 'err'}))
            """

    async def set_manual(self, event):
        global list_channel_name, registered_clients
        global TERMINAL_GROUP_NAME
        if event['handler'] == 'asistencia':

            if event['action'] == 'connect':
                connected = 1
            else:
                connected = 0
            
            pais = event['name']

            i = len(registered_clients)//2
            base = 0
            tope = len(registered_clients)-1

            while registered_clients[i].pais != pais and (base <= tope):
                if registered_clients[i].pais > pais:
                    tope = i - 1
                else:
                    base = i + 1
                i = base + (tope - base) // 2

            channel_layer = get_channel_layer()

            if event['action'] == 'disconnect':
                await channel_layer.send(str(registered_clients[i].channel_name), {'type': 'disconnect'})
                
            registered_clients[i].channel_name = None
            registered_clients[i].connected = 1
            registered_clients[i].attended = 1
            registered_clients[i].manual = 1
            code = 'ok'
            try:
                #channel_name = Cliente.objects.get(descripcion='moderador')
                await self.channel_layer.group_send(
                    TERMINAL_GROUP_NAME,
                    {'type': 'notificate_terminal', 'name':pais, 'subject': 'register', 'connected': connected, 'attended':connected, 'manual': 1}
                )
                """ 
                for i in list_channel_name:
                    await channel_layer.send(str(i.channel_name), {'type': 'notificate_terminal', 'name':pais, 'subject': 'register', 'connected': connected, 'attended':connected, 'manual': 1}) """

            except Exception:
                raise

        elif event['handler'] == 'votacion':
            global last_poll
            v = Votacion.objects.all().latest('idVotacion')
            pais = event['pais']
            selected = event['selected']
            try:
                votoAnterior = Voto.objects.get(idVotacion=v, pais=pais)
            except Voto.DoesNotExist:
                votoAnterior = None
            if votoAnterior:
                votoAnterior.voto = event['selected']
                votoAnterior.save()
            else:
                Voto.objects.create(idVotacion=v, pais=event['pais'], voto=event['selected'])
            try:
                # buscar en la estucrtura de votos en donde está
                # y setear el voto
                index = None
                for idx, country in enumerate(last_poll['noVotaron']):
                    if country[0] == pais:
                        index = idx
                        break

                if index is not None:
                    last_poll['noVotaron'][index][1] = 0


                    if selected == 'yes':
                        last_poll['aFavor'][index][1] = 1
                        last_poll['enContra'][index][1] = 0
                        last_poll['abstencion'][index][1] = 0
                        
                    elif selected == 'no':
                        last_poll['aFavor'][index][1] = 0
                        last_poll['enContra'][index][1] = 1
                        last_poll['abstencion'][index][1] = 0
                    elif selected == 'abs':
                        last_poll['aFavor'][index][1] = 0
                        last_poll['enContra'][index][1] = 0
                        last_poll['abstencion'][index][1] = 1

                
                resp = {'type': 'notificate_terminal', 
                            'aFavor':last_poll['aFavor'],
                            'noVotaron':last_poll['noVotaron'],
                            'enContra':last_poll['enContra'],
                            'abstencion':last_poll['abstencion'],
                            'subject':'new_voto',}
                print('', flush=True)
                await self.channel_layer.group_send(
                    TERMINAL_GROUP_NAME,
                    resp
                )

            except Exception as e:
                pass

        elif event['handler'] == 'oradores':

            if 'id' in event:
                if event['id'] is not None:
                    if event['id'] == '-1':
                        try:
                            lista = ListaOradores.objects.latest()
                        except Exception:
                            return 0
                    else:
                        lista = ListaOradores.objects.get(id=event['id'])
                else:
                    return 0
            else:
                return 0
            try:
                dAnotada = DelegacionOradora.objects.get(pais=event['pais'], lista=lista)
            except DelegacionOradora.DoesNotExist:
                dAnotada = None
            if dAnotada:
                return 0
            else:
                DelegacionOradora.objects.create(pais = event['pais'], lista=lista, hablo=0)

            try:
                #channel_name = Cliente.objects.get(descripcion='moderador')
                await self.channel_layer.group_send(
                    TERMINAL_GROUP_NAME,
                    {'type': 'notificate_terminal', 'name':event['pais'], 'selected':'yes', 'subject': 'oradores', 'spoken':0}
                )

            except Exception as e:
                raise
                pass


    async def set_actividad(self, event):
        try:
            try:
                dia = DiaCronograma.objects.get(fecha=event['form']['fecha'])
            except DiaCronograma.DoesNotExist:
                dia = DiaCronograma()
                dia.fecha = event['form']['fecha']
                dia.save()
            a = ActividadCronograma()
            a.titulo = event['form']['titulo']
            a.texto = event['form']['texto']
            a.hora = event['form']['hora']
            a.fecha = dia
            a.save()
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'result_actividad'}))
        except Exception:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'result_actividad'}))

    async def update_actividad(self, event):
        try:
            try:
                dia = DiaCronograma.objects.get(fecha=event['form']['fecha'])
            except DiaCronograma.DoesNotExist:
                dia = DiaCronograma()
                dia.fecha = event['form']['fecha']
                dia.save()
            a = ActividadCronograma.objects.get(id=event['form']['id'])
            a.titulo = event['form']['titulo']
            a.texto = event['form']['texto']
            a.hora = event['form']['hora']
            a.fecha = dia
            a.save()
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'result_actividad'}))
        except Exception:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'result_actividad'}))


    async def get_actividades(self, event):
        dias = DiaCronograma.objects.all().order_by('-fecha')
        resp = []
        for d in dias:
            dia = {'fecha': str(d.fecha)}
            actividades = ActividadCronograma.objects.filter(fecha = d)
            serializer = ActividadCronogramaSerializer(actividades, many=True)
            dia['actividad'] = serializer.data
            resp.append(dia)
        await self.send(text_data = json.dumps({'list': resp, 'subject':'get_actividades'}))

    async def delete_actividad(self, event):
        try:
            a = ActividadCronograma.objects.get(id=event['id'])
            a.delete()
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'result_actividad'}))
        except Exception as e:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'result_actividad'}))

    async def share_event(self, event):
        try:
            v = Votacion.objects.get(id=event['id'])
            v.mostrar = 1
            v.save()
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'share_event'}))

            for i in registered_clients:
                    if (i.connected and not i.manual):
                        await self.channel_layer.send(
                            i.channel_name,
                            {'type': 'get_events'}
                        )

        except:
            raise


    async def post_document(self, event):
        if not exists(f'{BASE_DIR}/anteproyecto'):
            mkdir(f'{BASE_DIR}/anteproyecto')
        if not exists(f'{BASE_DIR}/reglamentos'):
            mkdir(f'{BASE_DIR}/reglamentos')
        try:
            if (event['titulo'] == "Anteproyecto de Resolucion"):
                f = open(f'{BASE_DIR}/anteproyecto/'+event['titulo']+'.pdf', 'wb')
                data = event['documento'][len('data:application/pdf;base64,'):]
                f.write(base64.b64decode(data))
                f.close()
                await self.send(text_data = json.dumps({'status':'ok', 'subject':'post_document'}))
            else:
                if path.exists(f'{BASE_DIR}/reglamentos/'+event['titulo']+'.pdf'):
                    await self.send(text_data = json.dumps({'status':'err', 'subject':'post_document', 'msge':'Ya existe un archivo con ese nombre'}))
                    return 0
                f = open(f'{BASE_DIR}/reglamentos/'+event['titulo']+'.pdf', 'wb')
                data = event['documento'][len('data:application/pdf;base64,'):]
                f.write(base64.b64decode(data))
                f.close()
                await self.send(text_data = json.dumps({'status':'ok', 'subject':'post_document'}))
        except Exception:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'post_document', 'msge': 'Ha ocurrido un error'}))
    
    async def get_documents_list(self, event):
        if not exists(f'{BASE_DIR}/anteproyecto'):
            mkdir(f'{BASE_DIR}/anteproyecto')
        if not exists(f'{BASE_DIR}/reglamentos'):
            mkdir(f'{BASE_DIR}/reglamentos')
        files = [f for f in listdir(f'{BASE_DIR}/reglamentos') if isfile(join(f'{BASE_DIR}/reglamentos', f))]
        resp = []
        if exists(f'{BASE_DIR}/anteproyecto/Anteproyecto de Resolucion.pdf'):
            resp.append({'titulo': 'Anteproyecto de Resolucion', 'link': 'http://'+ IPHOST +'/anteproyecto/Anteproyecto de Resolucion.pdf'})
        for f in files:
            resp.append({'titulo': f, 'link': 'http://'+ IPHOST +'/reglamentos/'+f})
        await self.send(text_data = json.dumps({'list':resp, 'subject':'get_documents_list'}))

    
    async def check_anteproyecto(self, event):
        if path.exists(f'{BASE_DIR}/anteproyecto/Anteproyecto de Resolucion.pdf'):
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'check_anteproyecto'}))
        else:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'check_anteproyecto'}))


    async def delete_document(self, event):
        try:
            if path.exists(f'{BASE_DIR}/reglamentos/'+event['titulo']):
                remove(f'{BASE_DIR}/reglamentos/'+event['titulo'])
                await self.send(text_data = json.dumps({'status':'ok', 'subject':'delete_document'}))
            else:
                await self.send(text_data = json.dumps({'status':'err', 'subject':'delete_document'}))
        except Exception:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'delete_document'}))


    async def get_comision(self, event):
        global comision
        if comision != None:
            await self.send(text_data = json.dumps({'comision':str(comision.numero), 'subject':'get_comision'}))
        else:
            await self.send(text_data = json.dumps({'comision':'0', 'subject':'get_comision'}))
    
    async def set_comision(self, event):

        try:
            c = Comision.objects.all()
            if len(c):
                if c[0].numero != event['comision']:
                    for i in registered_clients:
                        i.attended = 0
                        if (i.connected and not i.manual):
                            await self.channel_layer.send(
                                i.channel_name,
                                {'type': 'disconnect'})
                        i.manual = 0
            c.delete()
            global comision
            comision = Comision()
            comision.numero = int(event['comision'])
            comision.save()
            await self.send(text_data = json.dumps({'status':'ok', 'subject':'set_comision'}))
        except Exception:
            await self.send(text_data = json.dumps({'status':'err', 'subject':'set_comision'}))


    async def get_votacion(self, event):
        
        try:
            votacion = Votacion.objects.latest('id')
        except Exception:
            return 0

        votos = Voto.objects.filter(idVotacion=votacion)

        resp = {
            'aFavor':[],
            'noVotaron':[],
            'enContra':[],
            'abstencion':[],
            'subject':'get_votacion',
        }

        for voto in votos:
            if voto.voto == 'yes':
                resp['aFavor'].append((voto.pais, 1))
            elif voto.voto == 'no':
                resp['enContra'].append((voto.pais, 1))
            elif voto.voto == 'abs':
                resp['abstencion'].append((voto.pais, 1))

        for pais in registered_clients:
            if pais.attended:

                voto = None

                for voto in resp['aFavor'] + resp['enContra'] + resp['abstencion']:
                    if voto[0] == pais.pais:
                        voto = 1
                        break

                if not voto:
                    resp['noVotaron'].append((pais.pais, 1))
    
        print('', flush=True)

        await self.send(text_data = json.dumps(resp))


    async def alert_expired_time(self, event):
        await self.send(text_data=json.dumps(event))