from django.shortcuts import render, HttpResponse
from django.utils.safestring import mark_safe
import json
from django.views.decorators.csrf import csrf_exempt
import urllib.request as requests
from .models import Paises, Pais_Sede
import random, string


# Create your views here.

def index(request):
    return render(request, 'server/index.html', {})

def room(request, room_name, rol):
    return render(request, 'server/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)), 'rol': mark_safe(json.dumps(rol))
    })


@csrf_exempt
def get_app_data(request):
    url = "https://backend.sipum.drimit.net/getAppContext?token=" + request.GET.get('token')
    response = requests.urlopen(url)
    response_data = response.read()
    data = json.loads(response_data)

    Pais_Sede.objects.all().delete()
    

    paises = Paises.objects.filter(alternativo__in=data['data'])

    for pais in paises:
        ps = Pais_Sede() # ps es abreviatura de pais_sede
        ps.pais = pais.pais
        ps.save()

        print(data, pais.alternativo, flush=True)
        data['data'].remove(pais.alternativo.upper())
    
    for pais_no_registrado in data['data']:
        pais = Paises()
        pais.pais = pais_no_registrado.capitalize()
        pais.alternativo = pais_no_registrado
        codigo = ''.join(random.choices(string.ascii_letters + string.digits, k=3))
        while Paises.objects.filter(codigo=codigo.upper()).exists():
            codigo = ''.join(random.choices(string.ascii_letters + string.digits, k=3))

        pais.codigo = codigo.upper()
        pais.observadora = 1

        try:
            pais.save()
        except Exception:
            pass
        
    

    return HttpResponse(json.dumps(data), content_type='application/json')
    