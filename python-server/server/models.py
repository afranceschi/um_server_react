from django.db import models

# Create your models here.

class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=25, null=True)
    channel_name = models.CharField(max_length=250, null=True)

class Usuario(models.Model):
    token = models.CharField(max_length=12, null=False, primary_key=True)
    pais = models.CharField(max_length=30, null=False)
    comision = models.IntegerField(default=0)


class ListaOradores(models.Model):
    id = models.IntegerField(primary_key=True)
    asunto = models.CharField(max_length=250, default="")
    hora = models.DateTimeField(auto_now = True)

class DelegacionOradora(models.Model):
    pais = models.CharField(max_length=30, null=False)
    lista = models.ForeignKey(ListaOradores, on_delete=models.DO_NOTHING, null=True, default=None)
    hablo = models.IntegerField(default=0)

class Votacion(models.Model):
    idVotacion = models.IntegerField(null=False, default=0)
    asunto = models.CharField(max_length=250, null=False, default="")
    mostrar = models.IntegerField(default=0)

class VotacionProxy(Votacion):
    aFavor = 0
    enContra = 0
    abstencion = 0


class Voto(models.Model):
    idVotacion = models.ForeignKey(Votacion, null=False, on_delete=models.DO_NOTHING, default=0)
    voto = models.CharField(max_length=150, null=False)
    pais = models.CharField(max_length=150, null=False)

class Paises(models.Model):
    id = models.IntegerField(primary_key=True)
    pais = models.CharField(max_length=30, null=False)
    alternativo = models.CharField(max_length=30, null=False, default="")
    codigo = models.CharField(max_length=3, null=False)
    observadora = models.IntegerField(default=0)

class Evento(models.Model):
    id = models.AutoField(primary_key=True)
    mensaje = models.CharField(max_length=100)
    hora = models.IntegerField(null=False)
    tipo = models.CharField(max_length=100)

class DiaCronograma(models.Model):
    fecha = models.DateField()


class ActividadCronograma(models.Model):
    titulo = models.CharField(max_length=100)
    texto = models.CharField(max_length=100)
    hora = models.CharField(max_length=100)
    fecha = models.ForeignKey('DiaCronograma', on_delete=models.DO_NOTHING)

class Comision(models.Model):
    numero = models.IntegerField(default=0)

class Pais_Sede(models.Model):
    id = models.AutoField(primary_key=True)
    pais = models.CharField(max_length=30, null=False)