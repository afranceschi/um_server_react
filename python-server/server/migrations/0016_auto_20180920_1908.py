# Generated by Django 2.0.6 on 2018-09-20 19:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0015_auto_20180817_0237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='descripcion',
            field=models.CharField(max_length=25, null=True),
        ),
    ]
