# Generated by Django 2.0.6 on 2018-08-10 20:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0009_auto_20180810_2011'),
    ]

    operations = [
        migrations.AddField(
            model_name='voto',
            name='idVotacion',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.DO_NOTHING, to='server.Votacion'),
        ),
        migrations.AlterField(
            model_name='voto',
            name='pais',
            field=models.CharField(max_length=150),
        ),
        migrations.AlterField(
            model_name='voto',
            name='voto',
            field=models.CharField(max_length=150),
        ),
    ]
