# Generated by Django 2.0.6 on 2018-10-01 21:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0017_actividadcronograma'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiaCronograma',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dia', models.DateField()),
                ('actividad', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='server.ActividadCronograma')),
            ],
        ),
    ]
