from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/server/(?P<room_name>[^/]+)/(?P<rol>[^/]+)/$', consumers.ServerConsumer),
]
