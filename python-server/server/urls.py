from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<room_name>[^/]+)/(?P<rol>[^/]+)/$', views.room, name='room'),
    url(r'^getAppData/$', views.get_app_data, name='getAppData'),
]
