import sqlite3

conn = sqlite3.connect('db.sqlite3')
c = conn.cursor()


tablas = ['server_comision', 
        'server_delegacionoradora', 
        'server_listaoradores', 
        'server_voto', 
        'server_votacion', 
        'server_actividadcronograma',
        'server_diacronograma',
        'server_evento',
        'server_pais_sede']

for i in tablas:
    query = "delete from %s;" % (i)
    c.execute(query)
    print("%s limpia" % i)

conn.commit()
conn.close()